<?php 
$c = Page::getCurrentPage();
$page = Page::getByID($c->getCollectionParentID());
$parent_url = View::URL($page->getCollectionPath());
echo '<p><a href="' . $parent_url . '" class="toParent">&laquo; Back</a></p>';
?>