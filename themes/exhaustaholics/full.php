<?php  defined('C5_EXECUTE') or die(_("Access Denied.")); ?>
<?php
	$this->inc('elements/header.php');
?>
<section class="mainContent grid_12">
	<?php
		$a = new Area('Main');
		$a->display($c);
	?>
</section>
<?php
	$this->inc('elements/footer.php');
?>