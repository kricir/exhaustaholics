<?php  defined('C5_EXECUTE') or die(_("Access Denied.")); ?>
<?php
	$this->inc('elements/header.php');
?>
<section class="mainContent grid_12">
	<?php
		print $innerContent;
	?>
</section>
<?php
	$this->inc('elements/footer.php');
?>