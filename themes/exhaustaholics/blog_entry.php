<?php  defined('C5_EXECUTE') or die(_("Access Denied.")); ?>
<?php
	$this->inc('elements/header.php');
?>
<div id="header-image">
        
        <div class="" id="featured-image-full">
            <?php
            
            if ($c->isEditMode()) {
                print '<br><br>';
                $a = new Area('Thumbnail Image');
                $a->display($c);
            }
            ?>
        </div>
        
    </div>
<section class="mainContent grid_9 push_3">
    <h1><?php echo $c->getCollectionName(); ?></h1>
                    
    <span class="meta">
        <?php 
            $u = new User();
            if ($u->isRegistered()) { ?>
                <?php  
                if (Config::get("ENABLE_USER_PROFILES")) {
                    $userName = '<a href="' . $this->url('/profile') . '">' . $u->getUserName() . '</a>';
                } else {
                    $userName = $u->getUserName();
                }
            }
            echo 'Posted by: <span class="post-author">' . $userName . ' on ' . $c->getCollectionDatePublic('F jS, Y') . '</a></span>';
        ?>
    </span>

    <?php 
        $a = new Area('Main');
        $a->display($c);
    ?>
</section>
<aside class="sidebar grid_3 pull_9">
    <?php
        $this->inc('elements/sidebar.php');
        if ($c->getAttribute('sidebar_ads') > 0 ? 1 : 0) {
    ?>
        <section class="sidebarAds">
            <?php
                $sidebarAds = new GlobalArea('Sidebar Ad');
                $sidebarAds->setBlockLimit(1);
                $sidebarAds->disableControls();
                $sidebarAds->display();
            ?>
        </section>
    <?php
        }
    ?>
</aside>
<?php
	$this->inc('elements/footer.php');
?>