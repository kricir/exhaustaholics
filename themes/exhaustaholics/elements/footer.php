<?php  defined('C5_EXECUTE') or die(_("Access Denied."));
			if ($c->getAttribute('footer_nav') > 0 ? 1 : 0) {
		?>
				<section class="footerAds grid_12">
					<?php
						$footerAds = new GlobalArea('Footer Ad');
						$footerAds->setBlockLimit(1);
						$footerAds->disableControls();
						$footerAds->display();
					?>
				</section>
		<?php
			}
		?>
		<p>Hello World</p>
		</div>
	</section>
	<section class="social">
		<div class="container_12">
			<figure class="lowerLogo grid_4">
				<?php
					$smallLogo = new GlobalArea('Footer Box');
					$smallLogo->setBlockLimit(1);
					$smallLogo->disableControls();
					$smallLogo->display();
				?>
			</figure>
			<section class="socialIcons grid_4 push_4">
				<?php
					$social = new GlobalArea('Social Icons');
					$social->setBlockLimit(1);
					$social->disableControls();
					$social->display();
				?>
			</section>
		</div>
	</section>
    <footer>
    	<div class="container_12">
    		<nav class="footerNav grid_12">
    			<?php
    				$footerNav = new GlobalArea('Footer Nav');
    				$footerNav->setBlockLimit(1);
    				$footerNav->disableControls();
    				$footerNav->display();
    			?>
    		</nav>
	    	<article class="copy grid_12">
	        	<p>Copyright &copy;<?php echo date('Y'); ?> <a href="http://www.scope10.com" title="Scope 10 - Mobile App Development & Web Services" target="_blank">Scope 10</a> - <a href="http://www.scope10.com" title="Scope 10 - Mobile App Development & Web Services" target="_blank">Mobile App Development</a> & <a href="http://www.scope10.com" title="Scope 10 - Mobile App Development & Web Services" target="_blank">Web Services</a> and <a href="http://www.kricir.com" title="Kricir Media Company - Minneapolis/St. Paul Web Design and Development" target="_blank">Kricir Media Company</a> - <a href="http://www.kricir.com" title="Kricir Media Company - Minneapolis/St. Paul Web Design and Development" target="_blank">Web Design and Development</a></p>
	        </article>
	    </div>
    </footer>
<?php
	Loader::element('footer_required');
?>
</body>
</html>