<?php  defined('C5_EXECUTE') or die(_("Access Denied.")); ?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo LANGUAGE?>" lang="<?php echo LANGUAGE?>" class="no-js"><head>
<?php Loader::element('header_required'); ?>
<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1" />
<script type="text/javascript" src="<?php echo $this->getThemePath(); ?>/scripts/modernizr.min.js"></script>
<link rel="stylesheet" media="screen" type="text/css" href="<?php echo $this->getThemePath(); ?>/styles/screen.css" />
<link rel="stylesheet" media="screen" type="text/css" href="<?php echo $this->getStyleSheet('typography.css')?>" />
<script type="text/javascript">
	function iphone() {
		window.scrollTo(0, 1);
	}
</script>
</head>
<body onload="iphone()">
	<header>
    	<div class="container_12">
    		<section class="logo grid_4">
    			<?php
    				$logo = new GlobalArea('Site Name');
    				$logo->setBlockLimit(1);
    				$logo->disableControls();
    				$logo->display();
    			?>
    		</section>
    		<section class="search grid_4 push_4">
    			<?php
    				$search = new GlobalArea('Search');
    				$search->setBlockLimit(1);
    				$search->disableControls();
    				$search->display();
    			?>
    		</section>
    		<nav class="mainNav grid_12">
    			<?php
    				$mainNav = new GlobalArea('Header Nav');
    				$mainNav->setBlockLimit(1);
    				$mainNav->disableControls();
    				$mainNav->display();
    			?>
    		</nav>
    	</div>
    </header>
    <section class="trunk">
    	<div class="container_12">