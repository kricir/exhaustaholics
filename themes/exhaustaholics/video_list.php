<?php  defined('C5_EXECUTE') or die(_("Access Denied.")); ?>
<?php
	$this->inc('elements/header.php');
?>
<section class="mainContent grid_9 push_3">
	<?php
		$a = new Area('Main');
		$a->display($c);
	?>
</section>
<aside class="sidebar grid_3 pull_9">
	<?php
		$this->inc('elements/sidebar.php');
		if ($c->getAttribute('sidebar_ads') > 0 ? 1 : 0) {
	?>
		<section class="sidebarAds">
			<?php
				$sidebarAds = new GlobalArea('Sidebar Ad');
				$sidebarAds->setBlockLimit(1);
				$sidebarAds->disableControls();
				$sidebarAds->display();
			?>
		</section>
	<?php
		}
	?>
</aside>
<?php
	$this->inc('elements/footer.php');
?>