<?php 
  defined('C5_EXECUTE') or die(_("Access Denied."));
?>

<div class="ccm-jereme-tweetcrete-edit-feed">
  <h1><?php  echo t('Edit Twitter Feed') ?></h1>
  <?php  $this->inc('jereme_tweetcrete_form.php') ?>
</div>