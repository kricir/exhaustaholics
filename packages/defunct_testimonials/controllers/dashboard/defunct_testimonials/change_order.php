<?php   

defined('C5_EXECUTE') or die(_("Access Denied."));

class DashboardDefunctTestimonialsChangeOrderController extends Controller {

	/*
	 * Passes testimonial list to the view
	 */	
	public function view() {
		$html = Loader::helper('html');
		$this->addHeaderitem($html->css(DIR_REL.'/packages/defunct_testimonials/stylesheets/dashboard.css'));
		
		if($this->getList()) {
			$this->set('testimonials', $this->getList());
		}
	}
	
	/*
	 * Provides a list of testimonials from the database
	 */
	private function getList() {
		$db = Loader::db();
		$q = "SELECT pID, author FROM pkTestimonials ORDER BY sort";
		$r = $db->query($q);
		
		if ($r && $r->numRows() > 0) {
			
			while ($row = $r->fetchRow()) {
				$rows[] = $row;
			}
			
			return $rows;
		} else {
			return FALSE;
		}
	}
	
	/*
	 * Updates the order of the testimonials and returns a message.
	 */
	public function ajax() {
		$db = Loader::db();
		
		$update_sort_array = $_POST['sort'];		
		$list_counter = 1;
		
		foreach ($update_sort_array as $pID) {
			$q = "UPDATE pkTestimonials SET sort = " . $list_counter . " WHERE pID = " . $pID;
			$r = $db->query($q);
		 	$list_counter = $list_counter + 1;
		}
		
		if($r){
		 	echo '<div class="block-message alert-message success"><p>'.t('Order updated').'</p></div>';
		} else {
			echo '<div class="block-message alert-message error"><p>'.t('Unable to update order').'</p></div>';
		}
				
		exit; // required or full page will render
	}		
}