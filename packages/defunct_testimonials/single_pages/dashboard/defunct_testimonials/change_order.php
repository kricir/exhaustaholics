<?php   defined('C5_EXECUTE') or die(_("Access Denied.")); ?>


<h1><span><?php   echo t('Change Testimonial Order')?></span></h1>

<div class="ccm-dashboard-inner ccm-ui">	

	<h2><?php   echo t('Drag and Drop Your Testimonials to Change Their Order'); ?></h2>

	<!-- UPDATED VIA AJAX -->
	<div id="status"></div>	
	
	<ul id="testimonial-order">
	<?php   foreach($testimonials as $t): ?> 
		<li id="sort_<?php   echo $t['pID']; ?>"><?php   echo htmlspecialchars($t['author']) ?></li>
	<?php   endforeach; ?>
	</ul>
</div>

<script type="text/javascript">
// drag and drop change_order.php dashboard js
$(document).ready(function(){ 
						   
	$(function() {
		$("#testimonial-order").sortable({ opacity: 0.6, cursor: 'move', update: function() {
			var order = $(this).sortable("serialize"); 
			$.post("<?php   echo $this->action('ajax'); ?>", order, function(theResponse){		
				$("#status").html(theResponse);				
			}); 															 
		}								  
		});
	});
 
});	
</script>