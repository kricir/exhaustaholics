<?php   
defined('C5_EXECUTE') or die(_("Access Denied."));
$ih = Loader::helper('concrete/interface');
$tinymce = $html->javascript('tiny_mce/tiny_mce.js');
$this->addHeaderItem($tinymce, 'CONTROLLER');  

	if($_POST['add_testimonial']) {
	
		if (!$error) {
			// add the testimonial
			$db = Loader::db();
			
			// get latest sort id
			$sort_id = $db->query("SELECT sort FROM pkTestimonials ORDER BY sort DESC LIMIT 1");
			// add 1 to the highest id
			$sort_id = $sort_id->fields['sort']+1;			
					
			$v = array($_POST['author'], $_POST['testimonial'], $_POST['optionalContent'], $sort_id);
			$r = $db->prepare("INSERT INTO pkTestimonials (author, testimonial, optionalContent, sort) VALUES (?, ?, ?, ?)");
			$res = $db->execute($r, $v);
			
			if ($res) {
			 	$this->controller->redirect('/dashboard/defunct_testimonials/manage/?message=2');
			}
		
		}	
	}
?>

<h1><span><?php   echo t('Testimonials')?></span></h1>

<div class="ccm-dashboard-inner ccm-ui">

	<?php 
		// Display success messages
		if($_GET['message']) {
		
			$display = '';
			
			switch(intval($_GET['message'])) {
				case 1:
					$display = t('Testimonial deleted');
					break;
				case 2:
					$display = t('Testimonial added');	
					break;
				case 3:
					$display = t('Testimonial updated');
					break;		
			}
			
			echo '<div class="block-message alert-message success"><p>'. $display .'</p></div>';
			
		}

	?>


	<!-- TESTIMONIALS LIST -->
	<table border="0" cellspacing="1" cellpadding="0" class="grid-list" width="600">
	<tr>
		<?php   if (count($testimonials) > 0) { ?>
			<td colspan="4" class="header"><?php   echo t('Current Testimonials')?></td>
		<?php   } else { ?>	
			<td colspan="4" class="header"><?php   echo t('No testimonials found. Please add one below')?></td>
		<?php   } ?>
	</tr>
	<?php   if (count($testimonials) > 0) { ?>
		<tr>
			<td class="subheader" width="400"><?php   echo t('Author')?></td>
			<td class="subheader"><?php   echo t('Text')?></td>
			<td class="subheader"></td>
			<td class="subheader"></td>
		</tr>
		
		<?php   foreach($testimonials as $t): ?> 
			<tr>
				<td><a href="<?php   echo $this->url('/dashboard/defunct_testimonials/manage/?task=edit&amp;pID='.$t['pID']).'#edit' ?>"><?php   echo htmlspecialchars($t['author']) ?></a></td>
				<td><p><?php   echo $t['testimonial'] ?></p></td>
				<td><?php   echo $ih->button(t('Edit'),$this->url('/dashboard/defunct_testimonials/manage/?task=edit&amp;pID=' . $t['pID'].'#edit'), 'left', false, array('title'=>t('Edit this testimonial')));?></td>
				<td><?php   
						$delConfirmJS = t('Are you sure you want to permanently remove this testimonial?');
						$pid = $t['pID'];
					 ?>
						<script type="text/javascript">
							deleteTestimonial<?php   echo $pid?> = function() {
								if (confirm('<?php   echo $delConfirmJS?>')) { 
									location.href = "<?php   echo $this->action('delete/?pID='.$pid)?>";				
								}
							}
						</script>
						<?php    print $ih->button_js(t('Delete Testimonial'), "deleteTestimonial$pid()", 'left');?>
				</td>
			</tr>
		<?php   endforeach; ?>
	<?php   } ?>
	</table>
</div>

	<!-- ADD / EDIT TESTIMONIAL -->
	<h1 id="edit"><span><?php   $_GET['task'] == 'edit' ? print t('Edit Testimonial') : print t('Add Testimonial'); ?></span></h1>
	
	<div class="ccm-dashboard-inner ccm-ui"> 
	
    	<div style="width: 750px;">
		<form method="post" id="add_testimonial_form" action="<?php   $_GET['task'] == 'edit' ? print $this->action('update') : print $this->url('/dashboard/defunct_testimonials/manage/')?>">
			<fieldset>
				<div class="clearfix">
					<label style="font-weight: bold;"><?php   echo t('Author')?><span class="required">*</span></label>
					<div class="input">
						<input type="text" name="author" class="span12" value="<?php   echo $author; ?>" />
					</div>
				</div>
				<div class="clearfix">
					<label style="font-weight: bold;"><?php   echo t('Testimonial')?><span class="required">*</span></label>
					
					<div class="input">
			            <?php  
			             // TinyMCE		
			    		 Loader::element('editor_init');
						 Loader::element('editor_config');
						 Loader::element('editor_controls');
			    		?>		    		
		            	<textarea name="testimonial" class="advancedEditor ccm-advanced-editor" style="width: 100%; height: 300px;"><?php   echo $testimonial; ?></textarea> 
		            </div>  
	            </div>
	            <div class="clearfix" style="padding-top: 15px;"	>
	            	<label style="font-weight: bold;"><?php  echo t('Optional Content') ?></label>           
		            <div class="input">
			            <p><?php   echo t('The content entered here will be displayed below the author. Links that start with <strong>http://</strong> will automatically link to their respective site. This is useful for adding additional content like Title, Country and Website for example:') ?></p>
			            <p>&mdash; Justin Frydman<br />
			            CEO of Defunct Inc.<br />
			            Calgary, AB, Canada<br />
			            Website: <a href="http://defunctlife.com">http://defunctlife.com</a></p>
						<textarea name="optionalContent" style="width: 100%; height: 100px;"><?php   echo $optionalContent; ?></textarea>
					</div>
				</div>
	           
				<?php   
				 $_GET['task'] == 'edit' ? $buttonText = t('Edit Testimonial') : $buttonText = t('Add Testimonial');
				 echo $ih->submit($buttonText, 'add_testimonial_form', 'left');
				 
				 if($_GET['task'] == 'edit') { ?><input type="hidden" value="1" name="edit_testimonial" /><input type="hidden" value="<?php   echo intval($_GET['pID']) ?>" name="pID" /><?php   } else { ?><input type="hidden" value="1" name="add_testimonial" /><?php   } ?>			
			</fieldset>
		</form>
        </div>	
	</div>