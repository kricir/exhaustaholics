<?php 
defined('C5_EXECUTE') or die(_("Access Denied."));

class PicmonkeyImageEditorException extends Exception { }

class PicmonkeyImageEditorPackage extends Package {

	protected $pkgHandle = 'picmonkey_image_editor';
	protected $appVersionRequired = '5.4.0';
	protected $pkgVersion = '0.95';

	public function getPackageDescription() {
		return t("This is a package to replace integrated image editor with Picmonkey, a powerful online Image Tool.");
	}

	public function getPackageName() {
		return t("Picmonkey Image Editor");
	}

	public function install() {

		try {
		//copy
			if(is_writable(DIR_BASE.'/elements')){
				if (!file_exists(DIR_BASE.'/elements/files/edit/')){
					mkdir(DIR_BASE.'/elements/files/edit', 0755, true);
				}
				if(is_writable(DIR_BASE.'/elements/files')){
					if(is_writable(DIR_BASE.'/elements/files/edit')){
						if(version_compare(APP_VERSION, '5.6', '<')) {
							$source = DIRNAME_PACKAGES . '/'.$this->pkgHandle.'/elements/files/edit/image55.php';
						} else {
							$source = DIRNAME_PACKAGES . '/'.$this->pkgHandle.'/elements/files/edit/image56.php';
						}
						$target = DIR_BASE.'/elements/files/edit/image.php';
						if(file_exists($target)){
							throw new PicmonkeyImageEditorException( t('%s already exists. Delete it and try again.', $target) );
						}else{
							if(!copy($source, $target)){
								throw new PicmonkeyImageEditorException( t('Could not copy image.php.') );
							}
						}	
					}else{
						throw new PicmonkeyImageEditorException( t('You need to make the folder %s writable.', DIR_BASE.'/elements/files/edit/') );
					}
				}else{
					throw new PicmonkeyImageEditorException( t('You need to make the folder %s writable.', DIR_BASE.'/elements/files/') );
				}
			}else{
				throw new PicmonkeyImageEditorException( t('You need to make the folder %s writable.', DIR_BASE.'/elements/') );
			}
		} catch (PicmonkeyImageEditorException $e) {
			throw $e;
		}
		$pkg = parent::install();
	}

	public function uninstall() {

		$pkg = parent::uninstall();

		$target = DIR_BASE.'/elements/files/edit/image.php';

		try {		
			if(file_exists($target)){
				if(!unlink($target)){
					throw new PicmonkeyImageEditorException( t('You should delete manually %s.', $target) );
				}
			}
		} catch (PicmonkeyImageEditorException $e) {
			throw $e;
		}

		/* Forbidden functions
		if(!rmdir(DIR_BASE.'/elements/files/edit/')){
			$message = t('You should delete manually %s.', DIR_BASE.'/elements/files/edit/');
			throw new PicmonkeyImageEditorException($message);
			exit;
		}
		if(!rmdir(DIR_BASE.'/elements/files/')){
			$message = t('You should delete manually %s.', DIR_BASE.'/elements/files/');
			throw new PicmonkeyImageEditorException($message);
			exit;
		}
		*/
	}
}