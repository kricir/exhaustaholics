<?php  
defined('C5_EXECUTE') or die(_("Access Denied."));
global $c;
?>
<div id="vimeo-area-<?php     echo $bID;?>" class="c5tp-no-text">
	<p><?php     echo $previewText; ?></p>
<?php     if ($c->isEditMode()) { ?>
	<div class="ccm-edit-mode-disabled-item" style="width:<?php     echo $videoWidth?>px; height:<?php     echo $videoHeight?>px;">
		<div style="padding:8px 0px; padding-top: <?php     echo round($viedoHeight/2)-10?>px;"><?php     echo t('Content disabled in edit mode.')?></div>
	</div>
<?php      }else{ ?>
	<div id="vimeo-player-<?php     echo $bID;?>" class="vimeo-player">
		<object width="<?php     echo $videoWidth;?>" height="<?php     echo $videoHeight ;?>"><param name="allowfullscreen" value="true" /><param name="allowscriptaccess" value="always" /><param name="movie" value="http://vimeo.com/moogaloop.swf?clip_id=<?php     echo $videoID;?>&amp;server=vimeo.com&amp;show_title=0&amp;show_byline=0&amp;show_portrait=0&amp;color=&amp;fullscreen=1" />

		<embed src="http://vimeo.com/moogaloop.swf?clip_id=<?php     echo $videoID;?>&amp;server=vimeo.com&amp;show_title=0&amp;show_byline=0&amp;show_portrait=0&amp;color=&amp;fullscreen=1" type="application/x-shockwave-flash" allowfullscreen="true" allowscriptaccess="always" width="<?php     echo $videoWidth;?>" height="<?php     echo $videoHeight;?>"></embed></object>
	</div>
<?php      } ?>
</div>