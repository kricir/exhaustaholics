<?php     defined('C5_EXECUTE') or die("Access Denied."); ?>

<style>
a.c5tp-pdf-link{
background-image: url(img src="<?php   echo DIR_REL?>/packages/pc5_custom_templates/blocks/file/templates/c5tp_download_file_pdf/icon.png ?>") />"
}
</style>

<?php  
	$f = $controller->getFileObject();
	$fp = new Permissions($f);
	if ($fp->canRead()) {
		$c = Page::getCurrentPage();
		if($c instanceof Page) {
			$cID = $c->getCollectionID();
		}
?>
<a class="c5tp-pdf-link" href="<?php   echo  View::url('/download_file', $controller->getFileID(),$cID) ?>"><?php   echo  stripslashes($controller->getLinkText()) ?></a>

<?php  
}
/*
$fo = $this->controller->getFileObject();?>
<a href="<?php   echo $fo->getRelativePath()?>"><?php   echo  stripslashes($controller->getLinkText()) ?></a>
*/
?>
