<?php         defined('C5_EXECUTE') or die(_("Access Denied."));

if(isset($large)){
	$size = '32';
} else {
	$size = '16';
}
?>

<!-- SOCIAL PROFILES -->
<div class="c5tp-social-icons-hover-effect<?php       if(isset($large)):?> large<?php       endif; ?>">
	<?php        foreach($profiles as $value=>$url): ?>
	<?php        if($url != null): ?>
		<span><a href="<?php        echo $url ?>"><img src="<?php        echo DIR_REL?>/packages/pc5_custom_templates/blocks/defunct_social_icons/templates/c5tp_social_icons_light/images/<?php        echo $value?>_<?php       echo $size?>.png" alt="<?php        echo $value?>" /></a></span>
	<?php       endif; ?>
	<?php       endforeach; ?>
</div>
<!-- / SOCIAL PROFILES -->