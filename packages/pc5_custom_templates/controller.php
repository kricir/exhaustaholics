<?php  

defined('C5_EXECUTE') or die(_("Access Denied."));

class Pc5CustomTemplatesPackage extends Package
{

	protected $pkgHandle = 'pc5_custom_templates';
	protected $appVersionRequired = '5.5';
	protected $pkgVersion = '3.1.2';

	public function getPackageDescription()
	{
		return t("A growing collection of custom templates for various blocks.");
	}

	public function getPackageName()
	{
		return t("PC5 Custom Templates");
	}

	public function install()
	{
		$pkg = parent::install();

		// install block
		BlockType::installBlockTypeFromPackage('pc5_custom_templates', $pkg);
	}
}