<?php   
	defined('C5_EXECUTE') or die(_("Access Denied."));
	Loader::model('event_item','proevents');
	Loader::model('eventify','proevents');
	global $c;
	
	$u = new User();
	
	Loader::model("attribute/categories/collection");
	$settings = Eventify::getSettings();
	$eventTitle = $c->getCollectionName();
	$eventDate = $c->getCollectionDatePublic($settings['date_format']);

	if($settings['search_path']){
		$nh = Loader::helper('navigation');
		$searchn= Page::getByID($settings['search_path']);
		$search= $nh->getLinkToCollection($searchn);
	}
	
	/* function for grabbing all related attributes */
	$atts = $c->getSetCollectionAttributes();
	foreach($atts as $attribute){
		$value = $c->getCollectionAttributeValue($attribute); 
		$handle = $attribute->akHandle;
		$$handle = $value;
	}
	
	$event_item = New EventItem($c,$_GET['eID']);
	$dates_array = $event_item->getEventItemDatesArray();
	$n = $event_item->getEventItemsNum();
	$next_dates_array = $event_item->getEventItemNextDates();
	?>
	<?php  
	if($u->isLoggedIn() && $settings['invites']==1){
	?>
	<div class="ccm-ui">
		<a href="javascript:;" onClick="inviteDialogDo();" class="invite btn info"><?php  echo t('Invite Others');?></a>
	</div>
	<?php  
	}
	?>
	<div class="event-attributes">
		<div>
			<h2><?php   echo $eventTitle; ?> </h2>
			<?php  
			if($settings['tweets']){
			?>
			<span class='st_twitter_hcount' displayText='Tweet'></span>
			<?php  
			}
			if($settings['fb_like']){
			?>
			<span class='st_facebook_hcount' displayText='Facebook'></span>
			<?php  
			}
			?>
			<script type="text/javascript">var switchTo5x=true;</script>
			<script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
			<script type="text/javascript">stLight.options({publisher:"<?php   echo $settings['sharethis_key'];?>"});</script>
			<?php   
			if (!empty($url)){
			
			echo '<i><u><a href="'.$url.'">'.$url.'</a></u></i>';
			
			}
			?>
			<br/><br/>
			<div class="date-times">
			<?php   
			if(is_array($next_dates_array)){
				foreach($next_dates_array as $next_date){
					$date = explode(':^:',$next_date);
					echo date('M dS ',strtotime($date[0]));
					if ($event_allday !=1){
					 	echo date(t('g:i a'),strtotime($date[1])).' - '.date(t('g:i a'),strtotime($date[2])).'<br/>';
					}else{
						echo ' - '.t('All Day').'<br/>';
					}
				}
			}
			?> 
			</div>
			<h5><?php   echo $event_local; ?>
			<?php   if($event_local && $address){?> - <?php   } echo $address; ?>
			</h5>
			<div id="deswrap">
				<div id="description">
					<?php   
					$content = $controller->getContent();
					print $content;
					?>
				</div>
			</div>
			<div id="eventfoot">
			<?php   if ($contact_email !=''){ echo '<a href="mailto:'.$contact_email.'">'.$contact_name.'</a>'; }?> <?php  if($contact_email && $address){ echo '|| ';}?> <?php  if($address !=''){ ?><a href="http://maps.google.com/maps?f=q&amp;hl=en&amp;&saddr=<?php   echo $address ;?>" target="_blank"> <?php   echo t('get directions')?></a> <?php   } ?>
			</div>
			<div id="tags">
			<?php   echo t('Tags')?>: <i>
			<?php   
				$ak_t = CollectionAttributeKey::getByHandle('event_tag'); 
				$tag_list = $c->getCollectionAttributeValue($ak_t);
				$akc = $ak_t->getController();

				if(method_exists($akc, 'getOptionUsageArray') && $tag_list){
					//$tags == $tag_list->getOptions();
			
						foreach($tag_list as $akct){
							$qs = $akc->field('atSelectOptionID') . '[]=' . $akct->getSelectAttributeOptionID();
							echo '<a href="'.BASE_URL.$search.'?'.$qs.'">'.$akct->getSelectAttributeOptionValue().'</a>&nbsp;&nbsp;';
								
						}
					
				}
			?>
			</i>
			</div>
		</div>
	</div>
	<?php  
	if($u->isLoggedIn() || $settings['invites']){
		$invite_AJAX = '';
		?>
		<div id="event_invite" style="display: none;" class="ccm-ui">
			<div id="email_message">
			
			</div>
			<div id="email_form">
				<?php   echo t('Please list each email on a new line.');?>
				<br/>
				<form name="event_invite" class="event_invite">
				<textarea name="invite_emails" class="invite_emails"></textarea>
				<br/>
				<input type="hidden" name="ccID" value="<?php   echo $c->getCollectionID();?>"/>
				<input type="hidden" name="uID" value="<?php   echo $u->getUserID();?>"/>
				<button name="send_invite" class="send_invite btn info"><?php   echo t('Send Invite');?></button>
				</form>
			</div>
		</div>
		<script type="text/javascript">
		/*<![CDATA[*/
		inviteDialogDo = function() {
			var el = document.createElement('div');
			el.id = "inviteDialogContent";
			el.className = "ccm-ui";
			el.innerHTML = $('#event_invite').html();
			el.style.display = "none";
			$('#event_invite').parent().append(el);
				jQuery.fn.dialog.open({
					title: 'Invite Others',
					element: '#inviteDialogContent',
					width: 300,
					modal: false,
					height: 120,
					onOpen:function(){ 
						$('.send_invite').click(function(){
							var url = '<?php   echo Loader::helper('concrete/urls')->getToolsURL('invite.php','proevents');?>?';
							var form = $('#inviteDialogContent .event_invite').serialize();
							$.get(url+form,function(response){
								//alert(response);
								if(response != 'success'){
									$('#inviteDialogContent #email_message').html('<div class="alert-message error"><p>'+response+'</p></div>');
								}else{
									$('#inviteDialogContent #email_form').hide();
									$('#inviteDialogContent #email_message').html('<div class="alert-message success"><p><?php   echo t('Your Invites were sent successfully!');?></p></div>');
								}
							});
							return false;
						});
					}
				});
		}
		/*]]>*/
		</script>
		<?php  
	}
	?>
	