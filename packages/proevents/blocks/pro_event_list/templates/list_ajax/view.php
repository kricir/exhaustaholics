<?php   
defined('C5_EXECUTE') or die(_("Access Denied."));
$bt = BlockType::getByHandle('pro_event_list');
$AJAX_url = BASE_URL.Loader::helper('concrete/urls')->getToolsURL('ajax_list.php','proevents');
global $c;
?>
<div id="event_filters">
	<select id="date_span" class="filters">
		<option value=""><?php echo t('All')?></option>
		<option value="Day"><?php echo t('Today')?></option>
		<option value="Week"><?php echo t('This Week')?></option>
		<option value="FollowingWeek"><?php echo t('Next Week')?></option>
		<option value="Month"><?php echo t('By Month')?></option>
		<option value="FollowingMonth"><?php echo t('Next Month')?></option>
	</select>
	<select id="date_jump" class="filters" style="display: none;">
		<option value=""><?php echo t('All')?></option>
		<?php 
		for($d=0;$d<12;$d++){
			$m = date('Y-m-d',strtotime('+'.$d.' month'));
			echo '<option value="'.$m.'">'.date('M, Y',strtotime($m)).'</option>';
		}
		?>
	</select>
	<select id="date_cat" class="filters">
		<option value="All Categories"><?php echo t('All Categories')?></option>
		<?php   
		$options = Eventify::getEventCats();
		foreach($options as $option){
			echo '<option value="'.$option['value'].'">'.$option['value'].' </option>';
		}
		?>	
	</select>
</div>

<center class="ajax_loader" style="display: none;"><img src="<?php  echo Loader::helper('concrete/urls')->getBlockTypeAssetsURL($bt, 'ajax-loader.gif')?>" alt="loading"/></center>

<div class="ccm-page-list" id="event_results">


</div>

<script type="text/javascript">
/*<![CDATA[*/
	$(document).ready(function(){
		$('.ajax_loader').show();
		var url = '<?php  echo $AJAX_url?>?bID=<?php  echo $bID?>';
		var args = {ccID: <?php  echo $c->getCollectionID();?>,joinDays: true}
		$.get(url,args,function(data){
			$('.ajax_loader').hide();
			$('#event_results').html(data);
		});
		
		$('.filters').change(function(){
			$('.ajax_loader').show();
			var span = $('#date_span option:selected').val();
			if(span=='Month'){
				$('#date_jump').show();
			}else{
				$('#date_jump').val('');
				$('#date_jump').hide();
			}
			var date = $('#date_jump option:selected').val();
			var cat = $('#date_cat option:selected').val();
			var args = {ccID: <?php  echo $c->getCollectionID();?>,type: span,category: cat,date: date,joinDays: true}
			$.get(url,args,function(data){
				$('.ajax_loader').hide();
				$('#event_results').html(data);
			});
		});
	});
	
	function getEventResults(obj,p){
		//$('#event_results').fadeOut('slow');
		$('#event_results').html('');
		$('.ajax_loader').show();
		var url = '<?php  echo $AJAX_url?>?bID=<?php  echo $bID?>';
		var args = {ccID: <?php  echo $c->getCollectionID();?>,currentPage:p,joinDays: true}
		$.get(url,args,function(data){
			$('.ajax_loader').hide();
			$('#event_results').html(data);
			//$('#event_results').fadeIn('slow');
		});
		return false;
	}
/*]]>*/
</script>