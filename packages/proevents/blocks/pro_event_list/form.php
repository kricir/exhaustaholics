<?php    
defined('C5_EXECUTE') or die(_("Access Denied."));
$fm = Loader::helper('form'); 
Loader::model('eventify','proevents');
global $c;
$cParentID =  $c->getCollectionID(); 
?>
<div id="ccm-pagelistPane-add" class="ccm-pagelistPane">
	<div class="ccm-block-field-group">
		<h2><?php    echo t('Event List Title');?></h2>
		 <?php   echo $fm->text('rssTitle', $rssTitle);?>
	</div>
	<div class="ccm-block-field-group">
		<h2><?php    echo t('Event Filters');?></h2>
	 	<div><strong><?php    echo t('Choose Event Category');?></strong></div>
    	<?php   
    		function getCat($ctID) {
    		
    		$selected_cat = explode(', ',$ctID);
			
			if(in_array('All Categories', $selected_cat) || empty($selected_cat)){
				echo '<input type="checkbox" name="ctID[]" value="All Categories" checked/> All Categories</br>';
			}else{
				echo '<input type="checkbox" name="ctID[]" value="All Categories"/> All Categories</br>';
			}
			
			$options = Eventify::getEventCats();
			
			foreach($options as $option){
				echo '<input type="checkbox" name="ctID[]" value="'.$option['value'].'"';
				if(in_array($option['value'], $selected_cat)){
					echo ' checked';
				}
				echo '/> '.$option['value'].' </br>';
			}
			
		}
		echo getCat($ctID);
	?>	
	
	<br/>

	 	<div><strong><?php    echo t('Choose Event Section');?></strong></div>
    	<select name="sctID">
    	<?php   
    	function getSec($sctID) {
			$db = Loader::db();
			Loader::model('page_list');
			$eventSectionList = new PageList();
			$f = "ak_event_section = 1";
			$ubp = Page::getByPath('/homegroups');
			if($ubp->cID > 0){
				$f .= " OR ak_homegroup_section = 1";
			}
			$ubp = Page::getByPath('/meetings');
			if($ubp->cID > 0){
				$f .= " OR ak_meeting_section = 1";
			}
			$eventSectionList->filter(false,$f);
			$eventSectionList->sortBy('cvName', 'asc');
			//$eventSectionList->debug();
			$tmpSections = $eventSectionList->get();
			$sections = array();
			foreach($tmpSections as $_c) {
				$section = $_c->getCollectionName();

				if ($_c->getCollectionID() == $sctID && $section != 'All Sections'){
			  		echo '<option  value="'.$_c->getCollectionID().'"  selected>'.$section.'</option>';
				} elseif($section != 'All Sections') {
					echo  '<option value="'.$_c->getCollectionID().'">'.$section.'</option>';
				}
			}
			if ($sctID == 'All Sections' || $sctID == '' || !isset($sctID)){
				echo '<option value="All Sections" selected>'.t('All Sections').'</option>';
			} elseif($sctID != 'All Sections') {
				echo '<option value="All Sections">'.t('All Sections').'</option>';
			}
		}
		echo getSec($sctID);
	?>	
		</select>
	</div>
<br/>
<h2><?php    echo t('Event List Options');?></h2>
	<span>
	  	<div><strong><?php    echo t('Display message for "no events"');?></strong></div>
	  	<input type="text" name="nonelistmsg" value="<?php    if(isset($nonelistmsg)){echo $nonelistmsg;}?>" style="width: 250px">
	</span>
<br/><br/>	
	<span>
	  	<div><strong><?php    echo t('Number of Events');?></strong></div>
	  	<input type="text" name="num" value="<?php    if(isset($num)){echo $num;}else{echo '3';}?>" style="width: 30px">
	  	<span>
	  	 <input name="isPaged" type="checkbox" value="1" <?php   if ($isPaged == 1) {echo 'checked' ;}  ?> /> 
	  	 <?php    echo t('show pagination?')?>
	  	</span>
	</span>
<br/><br/>
	<span>
	   	<div><strong><?php    echo t('Truncate Descriptions');?></strong></div>  
	  	<input name="truncateSummaries" type="checkbox" value="1" <?php   if ($truncateSummaries == 1) {echo 'checked' ;}  ?> /> 
	   		<?php    echo t('Truncate Event Descriptions after');?> 
			<input type="text" name="truncateChars" size="3" value="<?php    if ($truncateChars){echo $truncateChars;}?>" /> 
			<?php    echo t('characters');?>
	</span>
<br/><br/>
	<span>
		<div><strong><?php   echo t('Ordering') ?></strong></div>
		<select name="ordering">
			<option value="ASC" <?php   if($ordering=='ASC'){echo 'selected' ;} ?> >Ascending</option>
			<option value="DESC" <?php   if($ordering=='DESC'){echo 'selected' ;} ?> >Descending</option>
		</select>
	</span>
<br/><br/>
	<span>
	   	<div><strong><?php    echo t('Show iCal Feed?');?></strong></div>  
	  	<input name="showfeed" type="checkbox" value="1" <?php   if ($showfeed == 1) {echo 'checked' ;}  ?> /> <?php    echo t('yes');?>
	</span>
	<?php   if (isset($rssDescription)) { echo '<input type="hidden" name="rssDescription" value="'.$rssDescription.'" />';} ?>