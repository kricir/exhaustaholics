<?php     
defined('C5_EXECUTE') or die(_("Access Denied."));

class Eventify Extends Model{	
	
	
	public function getSettings(){
		$db= Loader::db();
		$r = $db->execute("SELECT * FROM btProEventSettings");
		while($row=$r->fetchrow()){
			$settings = $row;
		}
		if (empty($settings)){$settings = array();}
		
		return $settings;
		
	}
	
	public function URLfix($link) {
		if (substr($link,-1)!='/'){
	    	return $link.'&';
		}else{
			return $link.'?';
		}
	}
	
	
	public function grabURL($cParentID, $fix=0) {
		$c = Page::getByID($cParentID);
		$n = Loader::helper('navigation');
		$page = $n->getLinkToCollection($c);
		if ($fix==1){
			return GrabPosts::URLfix($page);
		}else{
			return $page;
		}
	}
	
	public function getClickedDate($eID){
		$db = Loader::db();
		$r = $db->execute("SELECT * FROM btProEventDates WHERE eID = ?",array($eID));
		while($row = $r->fetchrow()){
			if($date != $row['date']){
				$dates[] = $row['date'].':^:'.$row['sttime'].':^:'.$row['entime'];
			}
			$date = $row['date'];
		}
		return $dates;
	}
	
	public function getNextNumDates($cID,$n){
		$db = Loader::db();
		$r = $db->execute("SELECT * FROM btProEventDates WHERE eventID = $cID AND date >= CURDATE() ORDER BY date,sttime ASC LIMIT $n");
		while($row = $r->fetchrow()){
			$dates[] = $row['date'].':^:'.$row['sttime'].':^:'.$row['entime'];
		}
		return $dates;
	}
	
	public function getDateSpan($category,$start,$end,$section=null){
		
		$categories = explode(', ',$category);
		
		if($categories != null && !in_array('All Categories',$categories)  && !in_array('',$categories)){
			$ccount = count($categories);
			$category = "AND ( category LIKE ";
			foreach($categories as $category_item){
				$category .= '\'%'.$category_item.'%\'';
				$cct++;
				if($cct < $ccount){
					$category .= ' OR category LIKE  ';
				}
			}
			$category .= ')';
		}else{
			$category = '';
		}
		
		if($section != null){$section = "AND section LIKE '$section'";}else{$section='';}
		$db = Loader::db();

		$events = array();
		$r = $db->Query("SELECT * FROM btProEventDates WHERE date >= DATE_FORMAT('$start','%Y-%m-%d') AND date <= DATE_FORMAT('$end','%Y-%m-%d') $category $section ORDER BY date ASC, sttime ASC");

		while($row=$r->fetchrow()){
			if(Eventify::checkDateExclude($row['eID']) == false){
				$cID = $row['eventID'];
				$co = Collection::getByID($cID);
				$cvo = $co->getVersionObject();
				if($cvo->isApproved() != true && $cID){
					$db->execute("DELETE FROM btProEventDates WHERE eventID = $cID");
				}else{
					$events[] = $row;
				}
			}
		}
		
		return $events;
	}
	
	
	public function checkDateExclude($eID){
		$db = Loader::db();
		
		$excluded = array();
		$r = $db->Query("SELECT * FROM btProEventDatesExclude WHERE eventID = $eID");
		
		if($r->RecordCount()>0){
		return true;
		}else{
			return false;
		}
	}
	
	public function getExcludedDates(){
		$db = Loader::db();
		
		$excluded = array();
		$r = $db->Query("SELECT * FROM btProEventDatesExclude");
		
		while($row=$r->fetchrow()){
			$excluded[] = $row['eventID'];
		}
		
		return $excluded;
	}
	
	public function getSearchEvents($search,$type){
	
	
		$db = Loader::db();
		
		$events = array();
		
		switch($type){
			case 'title':
				$r = $db->Query("SELECT * FROM btProEventDates WHERE title LIKE '%$search%' AND date >= CURDATE() ORDER BY date");
				break;
			
			case 'date':
				$date = date('Y-m-d',strtotime($search));
				$r = $db->Query("SELECT * FROM btProEventDates WHERE date = DATE_FORMAT('$date','%Y-%m-%d')");
				break;
				
			case 'description':
				$r = $db->Query("SELECT * FROM btProEventDates WHERE description LIKE '%$search%' AND date >= CURDATE() ORDER BY date");
				break;
		}

		while($row=$r->fetchrow()){
			$events[] = $row;
		}
			
		return $events;
	}
	
	public function getAllEvents(){
	
		$db = Loader::db();
		
		$events = array();
		$r = $db->Query("SELECT * FROM btProEventDates WHERE date >= CURDATE() ORDER BY date");

		while($row=$r->fetchrow()){
			$events[] = $row;
		}
			
		return $events;
	}
	
	public function getEvent($eID){
		$db = Loader::db();
		$r = $db->execute("SELECT * FROM btProEventDates WHERE eID = ?",array($eID));
		while($row = $r->fetchrow()){
			$date = $row;
		}
		return $date;
	}
	
	public function updateDate($vars){
		$db = Loader::db();
		$r = $db->execute("UPDATE btProEventDates SET title=?,description=? WHERE eID = ?",$vars);
	}
	
	
	public function getEventCats(){
		$db = Loader::db();
		$akID = $db->query("SELECT akID FROM AttributeKeys WHERE akHandle = 'event_category'");
		while($row=$akID->fetchrow()){
			$akIDc = $row['akID'];
		}
		$akv = $db->execute("SELECT value FROM atSelectOptions WHERE akID = $akIDc");
		while($row=$akv->fetchrow()){
			$values[]=$row;
		}
		if (empty($values)){
			$values = array();
		}
		return $values;
	}
	
	public function getRawEventID($cID,$date,$sttime,$entime){
		$start = strtoupper(date('g:i a',strtotime($sttime)));
		$end =  strtoupper(date('g:i a',strtotime($entime)));
		$db = loader::db();
		$eventID = $db->getOne("SELECT eID FROM btProEventDates WHERE eventID = $cID AND date = '$date' AND DATE_FORMAT(sttime,'%l:%i %p') = '$start' AND DATE_FORMAT(entime,'%l:%i %p') = '$end'");
		return $eventID;
	}
	
}