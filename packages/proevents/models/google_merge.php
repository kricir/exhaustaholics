<?php     
defined('C5_EXECUTE') or die(_("Access Denied."));

class GoogleMerge Extends Model{

	var $events_array = array();
	var $i;
		
	function __construct($bID,$xml_feeds=null,$date=null){
		Loader::model('block');
		$b = Block::getByID($bID);
		
		$this->parseEventsToArray($b);
		
		$feeds = $this->getFeeds($b,$xml_feeds,$date);
		
		
		if($feeds){
			foreach($feeds as $feed){
				$this->parseFeedToArray($feed,$date);
			}
		}

		$this->mergeFeeds();
	}
	
	function getFeeds($b,$xml_feeds=null,$date=null){
		if(!$date){
			$date = date('Y-m-d');
		}

		if($xml_feeds){
			$template = strtolower($b->getBlockFilename());
			
			//automatically determin span of dates to retrieve based on themplate name
			if(substr_count($template,'day') > 0){
				$span = date('Y-m-d\T',strtotime('+1 day',strtotime(date('Ymd',strtotime($date)))));
			}elseif(substr_count($template,'week') > 0){
				$span = date('Y-m-d\T',strtotime('+1 week',strtotime(date('Ymd',strtotime($date)))));
			}else{
				$span = date('Y-m-d\T',strtotime('+1 month',strtotime(date('Ymd',strtotime($date)))));
			}
		
			$feeds = explode(':^:',$xml_feeds);
			
			foreach($feeds as $feed_data){
				$feed_data = str_replace('#',urlencode('#'),$feed_data);
				$rArray[] = $feed_data.'&start-max='.$span.'00:01:00-08:00&start-min='.date('Y-m-d\T',strtotime($date)).'00:01:00-08:00';
			}
		}
		
		return $rArray;
	}
	
	function parseFeedToArray($feed,$date=null){
	
		$url = $feed;
		
		$vars_array = explode('&',$feed);
		$vars = array();
		if(is_array($vars_array)){
			foreach($vars_array as $var){
				$var_set = explode('=',$var);
				$vars[$var_set[0]] = $var_set[1];
			}
		}
		
		$feed_array = $this->events_array;
		
		$ch = curl_init();    // initialize curl handle
		curl_setopt($ch, CURLOPT_URL,$url); // set url to post to
		curl_setopt($ch, CURLOPT_RETURNTRANSFER,1); // return into a variable
		curl_setopt($ch, CURLOPT_TIMEOUT, 4); // times out after 4s
		$result = curl_exec($ch); // run the whole process
		//$feed_array[] = json_decode(json_encode((array) simplexml_load_string($result)),1);
		$s = new SimpleXMLElement($result); 
	
	    foreach ($s->entry as $item) {
	    	
	    	$this->i += 1;
	    	
	    	//var_dump(sprintf("%s",$item->content));
	    	//exit;
	    	
	   	 	$gd = $item->children('http://schemas.google.com/g/2005');
	
	        $startTime = '';
	        
	        if ( $gd->when ) {
	            $startTime = $gd->when->attributes()->startTime;
	        } elseif ( $gd->recurrence ) {
	
	        	//DTSTART;TZID=America/Grand_Turk:20120312T151000 DTEND;
	        	// 1-find text bweteen DTSTART <-> DTEND
	        	// 2-explode by ':' , second part of array is time stamp [1]
	        	// 3-wipe spaces
	        	if(preg_match_all('~DTSTART(.*?)DTEND~s',$gd->recurrence,$match)) {            
				        $m = explode(':',$match[1][0]);    
				        //var_dump($m[1]); 
				        $startTime = str_replace(' ','',$m[1]);    
				}
	            //$startTime = $gd->recurrence->when->attributes()->startTime; 
	        } 
	        if(!$date){
	        	$date = date('Ymd');
	        }else{
	        	$date = date('Ymd',strtotime($date));
	        }

	        
	        if(date('Ymd',strtotime($startTime)) >= $date){

		        $key = date('Ymd\THis',strtotime($startTime)).'_'.$this->i;
		
				$feed_array[$key]['datetime'] = date("l jS \o\f F Y - h:i A", strtotime( $startTime ) );
				
				$feed_array[$key]['title'] = sprintf("%s",$item->title);
				
				$feed_array[$key]['description'] = sprintf("%s",$item->content);
				
				$feed_array[$key]['link'] =  sprintf("%s",$item->link->attributes()->href);
		
		        if ( $gd->where ) {
		        	$feed_array[$key]['where'] = sprintf("%s",$gd->where->attributes()->valueString); 
		        }
		        
		        if($vars['color']){
		        	$feed_array[$key]['color'] = str_replace('%23','#',$vars['color']);
		        }
		    }
	    }
	    $this->events_array = $feed_array;
	}
	
	function parseEventsToArray($b){
		$dth = Loader::helper('form/date_time_time','proevents');
		$nh = Loader::helper('navigation');
		$controller = $b->getController();
		$events = $controller->getEvents(date('Y-m-d'));
		
		$feed_array = $this->feeds_array;
		
		if($events){
			foreach($events as $date_string=>$e){
				$this->i += 1;
				$location = $e->getAttribute('event_local');
				$date_array = $dth->translate_from_string($date_string);
				$eID = $date_array['eID'];
				$startTime = date('Ymd',strtotime($date_array['date'])).'T'.date('His',strtotime($date_array['start']));
				$key = $startTime.'_'.$this->i;
				$feed_array[$key]['datetime'] = date("l jS \o\f F Y - h:i A", strtotime( $startTime ) );
				$feed_array[$key]['title'] = $e->getCollectionName();
				$feed_array[$key]['description'] = $e->getCollectionDescription();
				$feed_array[$key]['link'] = $nh->getLinkToCollection($e).'?eID='.$eID;
				$feed_array[$key]['color'] = $e->getAttribute('category_color');
				if ( $location ) {
		        	$feed_array[$key]['where'] = $location; 
		        }
			}

			$this->events_array = $feed_array;
		}
	}
	
	function mergeFeeds(){
		ksort($this->events_array);
	}
	
	function getEventsArray(){
		return $this->events_array;
	}
}