<?php    
defined('C5_EXECUTE') or die("Access Denied.");
/**
*
* An object that allows a filtered list of events to be returned.
* @package ProEvents
*
**/
class EventItem extends Model {
	
	var $event;
	var $eID;
	var $num;
	var $dates_array;
	var $date_item;
	var $times_array;
	var $next_dates_array;
	
	
	function __construct($event, $eID = null){
		$this->event = $event;
		
		$dth = Loader::helper('form/date_time_time','proevents');
		$this->dates_array = $dth->translate_from($event);
		$this->num = count($this->dates_array);
		if($eID && $eID != ''){
			$this->eID = $eID;
			$this->setClickedDate();
		}else{
			$this->setNextNumDates();
		}
	
	}
	
	public function setItemFromString($string){
		$dth = Loader::helper('form/date_time_time','proevents');
		$this->date_array = $dth->translate_from_string($string);
		$this->eID = $this->date_array['eID'];
		$this->date_item = $this->date_array['date'];
		$times[0]['start'] = $this->date_array['start'];
		$times[0]['end'] = $this->date_array['end'];
		$this->times_array = $times;
	}
	
	
	public function getEventItemTimes(){
		$db = Loader::db();
		$times = array();
		$r = $db->execute("SELECT * FROM btProEventDates WHERE eventID = ? AND date = ?",array($this->event->getCollectionID(),$this->date_item));
		while($row = $r->fetchrow()){
			$i++;
			$times[$i]['start'] = $row['sttime'];
			$times[$i]['end'] = $row['entime'];
		}
		$this->times_array = $times;
		return $this->times_array;
	}
	
	private function setClickedDate(){
		$db = Loader::db();
		$r = $db->execute("SELECT * FROM btProEventDates WHERE eID = ?",array($this->eID));
		while($row = $r->fetchrow()){
			if($date != $row['date']){
				$dates[] = $row['date'].':^:'.$row['sttime'].':^:'.$row['entime'];
			}
			$date = $row['date'];
		}
		$this->next_dates_array = $dates;
	}
	
	private function setNextNumDates(){
		$db = Loader::db();
		$r = $db->execute("SELECT * FROM btProEventDates WHERE eventID = ? AND date >= CURDATE() ORDER BY date,sttime ASC LIMIT ?",array($this->event->getCollectionID(),$this->num));
		while($row = $r->fetchrow()){
			$dates[] = $row['date'].':^:'.$row['sttime'].':^:'.$row['entime'];
		}
		$this->next_dates_array = $dates;
	}
	
	public function getEventItemID(){return $this->eID;}
	public function getEventItemDate(){return $this->date_item;}
	public function getEventItemNextDates(){return $this->next_dates_array;}
	public function getEventItemsNum(){return $this->num;}
	public function getEventItemDatesArray(){return $this->dates_array;}
}


class EventItemDates extends model {

	var $event;
	var $title;
	var $description;
	var $allday;
	var $grouped;
	var $category;
	var $location;
	var $section;
	var $recur;
	var $eventID;
	var $dates;
	var $dates_array;
	
	function __construct($p,$save=false){

		Loader::model("collection");
		$cID = $p->getCollectionID();
		$event = Page::getByID($cID);
		
		//set the event object
		$this->event = $event;
		
		//set the event title
		$this->title = $event->getCollectionName(); 
		
		//set the event description
		$this->description = $event->getCollectionDescription(); 
		
		//set the allday
		$akad = CollectionAttributeKey::getByHandle('event_allday');
		$this->allday = $event->getCollectionAttributeValue($akad);
		
		//set grouped
		$akad = CollectionAttributeKey::getByHandle('event_grouped');
		$this->grouped = $event->getCollectionAttributeValue($akad);
		
		//set the dates array
		$dates_array = Loader::helper('form/date_time_time','proevents')->translate_from($event);
		sort($dates_array);
		$this->dates_array = $dates_array;
		
		//set event recuring
		$akrr = CollectionAttributeKey::getByHandle('event_recur');
		$this->recur = $event->getCollectionAttributeValue($akrr)->current()->value;
	
		//set event categories
		$aklc = CollectionAttributeKey::getByHandle('event_category');
		$eventCat_pre = $event->getCollectionAttributeValue($aklc);
		$ec = $event->getCollectionAttributeValue($aklc)->count();
		for($i=0;$i<$ec;$i++){
			$eventCat[]= $event->getCollectionAttributeValue($aklc)->current()->value;
			$event->getCollectionAttributeValue($aklc)->next();
		}
		if(is_array($eventCat_pre)){
			if(count($eventCat_pre)>1){
				$this->category = implode(', ',$eventCat_pre);
			}else{
				$this->category = $eventCat_pre[0];
			}
		}else{
			$this->category = $eventCat_pre;
		}
		
		//set event location
		$aklo = CollectionAttributeKey::getByHandle('event_local');
		$this->location = $event->getCollectionAttributeValue($aklo);

		//set event section
		$sec = Page::getByID($event->getCollectionParentID());
		$this->section = $sec->getCollectionID(); 
		

		$this->eventID = $event->getCollectionID();
		
		if($this->recur!=''){
			$this->dateSet();
		}else{
			//$dates = array($c->getCollectionDatePublic('Y-m-d')); 
			for($i=0; $i < count($this->dates_array); $i++){
				$dates[] = array(array('dsID'=> $this->dates_array[$i]['dsID'],'date'=>$this->dates_array[$i]['date']));
			}
			$this->dates = $dates;
		}
		
		//var_dump($this->dates);exit;
		if($save==true){
			$this->saveEventItemDates();
		}
	}
	
	private function dateSet(){		
$cobj = $this->event; $recur = $this->recur;Loader::model("attribute/categories/collection");$emdd = CollectionAttributeKey::getByHandle('event_multidate');$date_multi = $cobj->getCollectionAttributeValue($emdd);$date_multi_array = explode(':^:',$date_multi);foreach($date_multi_array as $dated){$date_sub = explode(' ',$dated);$dates_array[] = $date_sub[0];}sort($dates_array);$excluded_dates = array();$eexc = CollectionAttributeKey::getByHandle('event_exclude');$date_exclude = $cobj->getCollectionAttributeValue($eexc);$date_exclude_array = explode(':^:',$date_exclude);foreach($date_exclude_array as $exclude){$date_sub = explode(' ',$exclude);$excluded_dates[] = date('Y-m-d',strtotime($date_sub[0]));}$ess = date('Y-m-d',strtotime($dates_array[0]));$evth = CollectionAttributeKey::getByHandle('event_thru');$ee = date('Y-m-d',strtotime($cobj->getCollectionAttributeValue($evth)));$d1m = date('n',strtotime($cobj->getCollectionAttributeValue($evth)));$d1d = date('j',strtotime($cobj->getCollectionAttributeValue($evth)));$d1y = date('Y',strtotime($cobj->getCollectionAttributeValue($evth)));$d2m = date('n',strtotime($ess));$d2d = date('j',strtotime($ess));$d2y = date('Y',strtotime($ess));$datetime1 = mktime(0,0,0,$d1m,$d1d,$d1y);$datetime2 = mktime(0,0,0,$d2m,$d2d,$d2y);$interval = floor(($datetime1-$datetime2)/86400) ;$dayspan = $interval;$wk = true;for($d=0;$d<=$dayspan;$d+=1){$iti++;$year = $cobj->getCollectionDatePublic('Y');$month = $cobj->getCollectionDatePublic('m');$day = $cobj->getCollectionDatePublic('d');$daynum = date('Y-m-d',strtotime('+'.$d.' days', strtotime($ess)));if(!in_array($daynum,$excluded_dates)){if($recur==t('daily')){$di = 0;foreach($dates_array as $esd){$di++;$dates[$di][$iti]['date'] = $daynum;$dates[$di][$iti]['dsID'] = $di;}}elseif($recur==t('weekly')){$di = 0;foreach($dates_array as $esd){$di++;$es = date('Y-m-d',strtotime($esd));$eventDd = date('D',strtotime($es));$daynumD = date('D',strtotime($daynum));if($daynumD == $eventDd){$dates[$di][$iti]['date'] = $daynum;$dates[$di][$iti]['dsID'] = $di;}}}elseif($recur==t('every other week')){$di = 0;foreach($dates_array as $esd){$di++;$es = date('Y-m-d',strtotime($esd));$eventDd = date('D',strtotime($es));$daynumD = date('D',strtotime($daynum));if($daynumD == $eventDd && $wk==true){$dates[$di][$iti]['date'] = $daynum;$dates[$di][$iti]['dsID'] = $di;$wc++;if($wc == count($dates_array)){$wk = false;$wc = 0;}}elseif($daynumD == $eventDd && $wk==false){$wc++;if($wc == count($dates_array)){$wk = true;$wc = 0;}}}}elseif($recur==t('monthly')){$daynumm = date('Y-m',strtotime($daynum));$daynummDay = date('d',strtotime($daynum));$bug_array = array('01','08','15','22','29');$di = 0;foreach($dates_array as $esd){$di++;$es = date('Y-m-d',strtotime($esd));$esi = date('Y-m-d',strtotime($esd));$eventm = date('Y-m',strtotime($es));$eventD = date('d',strtotime($es));$eventDa = date('D',strtotime($es));$eventDaL = date('l',strtotime($es));$eventFirstDay = date('Y-m-d',strtotime($eventm.'-01'));$eventDa = date('D',strtotime($eventFirstDay));$monthFirstDay = date('Y-m-d',strtotime($daynumm.'-01'));$monthD = date('d',strtotime($monthFirstDay));$monthDa = date('D',strtotime($monthFirstDay));$eventFirstDay = date('Y-m-d',strtotime($eventm.'-01'));$em = date('m',strtotime($monthFirstDay));$bug_first = 'first ';$bug_second = 'second ';$bug_third = 'third ';$bug_fourth = 'fourth ';$bug_fifth = 'fifth ';if(in_array($daynummDay,$bug_array) && !in_array($eventD,$bug_array)){if($daynummDay == '01'){$bug_first = '+0 week ';$bug_second = 'first ';$bug_third = 'second ';$bug_fourth = 'third ';$bug_fifth = 'fourth ';$es = date('Y-m-d',strtotime('+0 week ', strtotime($es)));}else{$bug_first = 'first ';$bug_second = 'second ';$bug_third = 'third ';$bug_fourth = 'fourth ';$bug_fifth = 'fifth ';$es = date('Y-m-d',strtotime('-1 week ', strtotime($es)));}}if(in_array($eventD,$bug_array) && !in_array($daynummDay,$bug_array)){$es = date('Y-m-d',strtotime('+1 week ', strtotime($es)));}elseif(in_array($eventD,$bug_array) && $daynummDay == '01'){$bug_first = '+0 week ';$es = date('Y-m-d',strtotime('+0 week ', strtotime($es)));}if($es == date('Y-m-d',strtotime($bug_first.$eventDaL.'', strtotime($eventFirstDay)))){if($daynum == date('Y-m-d',strtotime($bug_first.$eventDaL.'', strtotime($monthFirstDay)))){$dates[$di][$iti]['date'] = $daynum;$dates[$di][$iti]['dsID'] = $di;}}elseif  ($es == date('Y-m-d',strtotime($bug_second.$eventDaL.'', strtotime($eventFirstDay)))){if($daynum == date('Y-m-d',strtotime($bug_second.$eventDaL.'', strtotime($monthFirstDay)))){$dates[$di][$iti]['date'] = $daynum;$dates[$di][$iti]['dsID'] = $di;}}elseif  ($es == date('Y-m-d',strtotime($bug_third.$eventDaL.'', strtotime($eventFirstDay)))){if($daynum == date('Y-m-d',strtotime($bug_third.$eventDaL.'', strtotime($monthFirstDay)))){$dates[$di][$iti]['date'] = $daynum;$dates[$di][$iti]['dsID'] = $di;}}elseif  ($es == date('Y-m-d',strtotime($bug_fourth.$eventDaL.'', strtotime($eventFirstDay)))){if($daynum == date('Y-m-d',strtotime($bug_fourth.$eventDaL.'', strtotime($monthFirstDay)))){$dates[$di][$iti]['date'] = $daynum;$dates[$di][$iti]['dsID'] = $di;}}elseif  ($es == date('Y-m-d',strtotime($bug_fifth.$eventDaL.'', strtotime($eventFirstDay)))){if($daynum == date('Y-m-d',strtotime($bug_fifth.$eventDaL.'', strtotime($monthFirstDay)))){$dates[$di][$iti]['date'] = $daynum;$dates[$di][$iti]['dsID'] = $di;}}}}}}$this->dates = $dates;
	}



	public function saveEventItemDates(){
		$db = Loader::db();
		$db->Execute("DELETE from btProEventDates where eventID = ?",array($this->eventID));
		$qi = ("INSERT INTO btProEventDates (title, category, section, eventID, date, allday, sttime, entime, description,location,grouped) VALUES (?,?,?,?,?,?,?,?,?,?,?)");

		$g = 0;
		
		foreach($this->dates as $date_days){
			if($this->grouped == 1){
				$g = 0;
			}
			foreach($date_days as $date){
				$g++;
				foreach($this->dates_array as $da){
					if($da['dsID'] == $date['dsID']){
					
						$start = $da['start'];
						$end = $da['end'];
						
						$args = array(
							'title'=> $this->title,
							'category'=> $this->category,
							'section'=> $this->section,
							'eventID'=> $this->eventID,
							'date'=> $date['date'],
							'allday'=> $this->allday,
							'sttime'=> date('H:i:s',strtotime($start)),
							'entime'=> date('H:i:s',strtotime($end)),
							'description'=> $this->description,
							'location'=> $this->location,
							'grouped'=>$g
						);
			
						
						$db->Execute($qi,$args);
					}
				}
			}
		}
	}
	
	
	public function getEventDates(){
		return $this->dates;
	}
	
}