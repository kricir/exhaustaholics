<?php   
defined('C5_EXECUTE') or die(_("Access Denied.")); 
	$dth = Loader::helper('form/date_time_time','proevents');
	$nh = Loader::helper('navigation');
	$bID = $_REQUEST['bID'];
	Loader::model('page');
	
	if($_REQUEST['state']){
		$c = Page::getByID($_REQUEST['cID'],$_REQUEST['state']);
	}else{
		$c = Page::getByID($_REQUEST['cID']);
	}

	$date =  date('Y-m-d',$_REQUEST['start']);
	$date2 = date('Y-m-d',$_REQUEST['end']);

	$blocks = $c->getBlocks();

	foreach($blocks as $b){
		if($b->getBlockTypeHandle()=='pro_event_list' && $b->bID == $bID){
			$controller = $b->getController();
		}
	}
	$events = $controller->getEvents($date,$date2);

	$recured_array = array();
	$events_array = array();

	foreach($events as $date_string => $ep){
		$i++;

  		$date_array = $dth->translate_from_string($date_string);
  		
  		$eID = $date_array['eID'];
  		$date = $date_array['date'];
  		$stime = $date_array['start'];
  		$etime = $date_array['end'];
  		
  		$id = $ep->getCollectionID();
  			
  		$title =  $ep->getCollectionName();
  		
		$url = $nh->getLinkToCollection($ep).'?eID='.$eID;
		if ($ep->getCollectionAttributeValue('exclude_nav')) {
			$url = '';
		}
		
		
		$content = null;
		if($ep->getCollectionDescription()){
			if($controller->truncateSummaries){
				$content = substr($ep->getCollectionDescription(),0,$controller->truncateChars).'...';
			}else{
				$content = $ep->getCollectionDescription();
			}
		}

		$location = $ep->getAttribute('event_local');
		
		$color = $ep->getAttribute('category_color');
		
		loader::model("attribute/categories/collection");
		$akrr = CollectionAttributeKey::getByHandle('event_recur');
		$recur = $ep->getCollectionAttributeValue($akrr)->current()->value;
		
		$allday = $ep->getAttribute('event_allday');
		
		if($recur == 'daily' && !in_array($id,$recured_array)){
			$dates = $dth->translate_from($ep);
			$thru = $location = $ep->getAttribute('event_thru');
			$from = $dates[1]['date'];
			$to = date('Y-m-d',strtotime($thru));

			array_push($recured_array,$id);
			
			if($allday == 1){
				$allday_text =  true;
			}else{
				$allday_text = false;
			}
	
			$event_item = array(
				'id' => $id,
				'title'=> $title,
				'allDay' => $allday_text,
				'start'=> $from.' '.date('H:i',strtotime($stime)).':00',
				'end' => $to.' '.date('H:i',strtotime($etime)).':00',
				'color' => $color,
				'url' => $url,
				'description' => $content
			);
			
			array_push($events_array,$event_item);
			
		}elseif(!in_array($id,$recured_array)){
			
			if($allday == 1){
				$allday_text =  true;
			}else{
				$allday_text = false;
			}
			
			$event_item = array(
				'id' => $id,
				'title'=> $title,
				'allDay' => $allday_text,
				'start'=> $date.' '.date('H:i',strtotime($stime)).':00',
				'end' => $date.' '.date('H:i',strtotime($etime)).':00',
				'color' => $color,
				'url' => $url
			);
			
			if($content){
				$event_item['description'] = $content;
			}
			
			array_push($events_array,$event_item);
		
		}
	}


   echo json_encode($events_array);


?>