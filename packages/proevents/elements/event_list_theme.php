<?php   
defined('C5_EXECUTE') or die(_("Access Denied."));

Loader::model('event_item','proevents');
$nh = Loader::helper('navigation');

$event_item = New EventItem($event);
$event_item->setItemFromString($date_string);

$eID = $event_item->getEventItemID();

$date = $event_item->getEventItemDate();

$times_array = $event_item->getEventItemTimes();
	
$title =  $event->getCollectionName();

$url = $nh->getLinkToCollection($event);
if ($event->getCollectionAttributeValue('exclude_nav')) {
	$url = 'javascript:;';
}

$content = $event->getCollectionDescription();

$allday = $event->getAttribute('event_allday');

$location = $event->getAttribute('event_local');

$color = $event->getAttribute('category_color');
	
$category = $event->getAttribute('event_category');

$contact_name = $event->getAttribute('contact_name');

$contact_email = $event->getAttribute('contact_email');

$address = $event->getAttribute('address');

$imgHelper = Loader::helper('image'); 
$imageF = $event->getAttribute('thumbnail');
if (isset($imageF)) { 
	$image = $imgHelper->getThumbnail($imageF, 110,85)->src; 
} 

$months = array(
	'Jan'=>t('Jan'),
	'Feb'=>t('Feb'),
	'Mar'=>t('Mar'),
	'Apr'=>t('Apr'),
	'May'=>t('May'),
	'Jun'=>t('Jun'),
	'Jul'=>t('Jul'),
	'Aug'=>t('Aug'),
	'Sep'=>t('Sep'),
	'Oct'=>t('Oct'),
	'Nov'=>t('Nov'),
	'Dec'=>t('Dec'),
);
//###################################################################################//
//here we lay out they way the page looks, html with all our vars fed to it	     //
//this is the content that displays.  it is recommended not to edit anything beyond  //
//the content parse.  Feel free to structure and re-arrange any element and adjust   //
//CSS as desired.							 	     //
//###################################################################################//

if($joinDays){
?>
<div class="smallcal">
	<div class="calwrap">
		<div class="img">
			<div class="month">
				<?php   
				$date_month = date('M', strtotime($date)) ; 
				echo $months[$date_month];
				?>
			</div>
			<div class="day">
				<?php   echo date('d', strtotime($date)) ; ?>
			</div>
		</div>
	</div>
	<div class="infowrap">
		<div class="titlehead">
			<?php   
			if($color){
				print '<div style="background-color: '.$color.';" class="category_color">'.$category.'</div>';
			}
			?>
			<div class="title">
			<?php   
				echo '<a href="'.$url.'?eID='.$eID.'">'.$title.'</a>' ; 	
			?>
			</div>
		   	<div class="local">
				<?php   echo $location ; ?>
			</div>
			<div class="time">
				<?php    
				if ($allday !=1){
                	if(is_array($times_array)){
	                	foreach($times_array as $time){
	                		print date('g:i a',strtotime($time['start'])).' - '.date('g:i a',strtotime($time['end'])).'<br/>';
	                	}
                	}
              	}else{
                	print t('All Day');
              	}   
              	?>
			</div>
		</div>
		<div class="description">
			<?php   
				if($imageF){
					echo '<div class="thumbnail">';
					echo '<img src="'.$image.'"/>';
					echo '</div>';
				}	
			?>
			<?php   
			if($truncateChars){
				print  substr($content,0,$truncateChars).'.....';
			}else{
				print  $content;
			}
			?>
		</div>
		<div class="eventfoot">
		<?php   
		if ($contact_email !=''){ ?>
			<a href="mailto:<?php   echo $contact_email ;?>"><?php   echo t('contact: ');?><?php   echo $contact_name?></a>
			<?php   } if ($contact_email !='' && $address !=''){ ?> || <?php   } if($address !=''){ ?><a href="http://maps.google.com/maps?f=q&amp;hl=en&amp;&saddr=<?php   echo $address ;?>" target="_blank"> <?php echo t('get directions')?></a> <?php   } ?>
		</div>
	</div>
</div>
<?php   
}else{
?>
<div class="smallcal sameday">
	<br class="clearfloat" />
	<div class="calwrap">
		
	</div>
	<div class="infowrap">
		<div class="titlehead">
			<div class="title">
			<?php   
				echo '<a href="'.$url.'">'.$title.'</a>' ; 	
			?>
			</div>
		   	<div class="local">
				<?php   echo $location ; ?>
			</div>
			<div class="time">
				<?php    
				if ($allday !=1){
                	if(is_array($times_array)){
	                	foreach($times_array as $time){
	                		print date('g:i a',strtotime($time['start'])).' - '.date('g:i a',strtotime($time['end'])).'<br/>';
	                	}
                	}
              	}else{
                	print t('All Day');
              	}   
              	?>
			</div>
		</div>
		<div class="description">
			<?php   
				if($imageF){
					echo '<div class="thumbnail">';
					echo '<img src="'.$image.'"/>';
					echo '</div>';
				}	
			?>
			<?php   
			if($truncateChars){
				print  substr($content,0,$truncateChars).'.....';
			}else{
				print  $content;
			}
			?>
		</div>
		<div class="eventfoot">
		<?php   
		if ($contact_email !=''){ ?>
			<a href="mailto:<?php   echo $contact_email ;?>"><?php   echo t('contact: ');?><?php   echo $contact_name?></a>
			<?php   } if ($contact_email !='' && $address !=''){ ?> || <?php   } if($address !=''){ ?><a href="http://maps.google.com/maps?f=q&amp;hl=en&amp;&saddr=<?php   echo $address ;?>" target="_blank"> <?php echo t('get directions')?></a> <?php   } ?>
		</div>
	</div>
</div>
<?php   	 			
//#####################################################################################//
//this is the end of the recommended content area.  please do not edit below this line //
//#####################################################################################//
}