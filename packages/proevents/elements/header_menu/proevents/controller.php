<?php   
defined('C5_EXECUTE') or die("Access Denied.");
class ProeventsConcreteInterfaceMenuItemController extends ConcreteInterfaceMenuItemController {
	
	public function displayItem() {
		$u = new User();
		if($u->isLoggedIn()){
			Loader::model('userinfo');
			$ui = UserInfo::getByID($u->uID);
			$u = new User();
			$isManager = $ui->getUserEventManager();
			if ($isManager || $u->isSuperUser()){
				if ($_SERVER['HTTPS'] != "on"){
					return true;
				}else{
					return false;
				}
			}
		}
		return false;
	}

}