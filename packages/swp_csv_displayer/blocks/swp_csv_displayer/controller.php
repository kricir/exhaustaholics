<?php  
defined('C5_EXECUTE') or die(_("Access Denied."));

class SwpCsvDisplayerBlockController extends BlockController {

	var $pobj;

	protected $btTable = 'btcsvimport';
	protected $btInterfaceWidth = "500";
	protected $btInterfaceHeight = "260";


	public function getBlockTypeDescription() {
		return "Create table from CSV<br />By <a href=\"http://www.smartwebprojects.net/\">www.smartwebprojects.net</a>";
	}

	public function getBlockTypeName() {
		return "CSV displayer";
	}

	public function __construct($obj = null) {
		parent::__construct($obj);
	}

	function getFileID() {return $this->fID;}
    function getFileObject() {
		return File::getByID($this->fID);
	}

	public function view(){
		$this->set('fID', $this->fID);
		$this->set('delimiter', $this->delimiter);
		$this->set('first_title', $this->first_title);
		$this->set('items_per_page', $this->items_per_page);
	}

	public function save($data) {
		$args = array();
		$args['fID'] = ($data['fID'] != '') ? $data['fID'] : 0;
		$args['delimiter'] = isset($data['delimiter']) ? $data['delimiter'] : '';
		$args['first_title'] = !empty($data['first_title']) ? 1 : 0;
		$args['items_per_page'] = !empty($data['items_per_page']) ? intval($data['items_per_page']) : 0;
		parent::save($args);
	}
}

?>