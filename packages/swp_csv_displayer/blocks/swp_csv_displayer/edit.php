<?php  
/*
Project: CSV Displayer block for concrete5 cms
Author: Anton Schepetkov
Group: Smart Web Projects
www.smartwebprojects.net
*/
defined('C5_EXECUTE') or die(_("Access Denied."));
$includeAssetLibrary = true;
$al = Loader::helper('concrete/asset_library');
$bf = null;
if ($controller->getFileID() > 0) {
	$bf = $controller->getFileObject();
}
?>
<h2><?php   echo 'CSV file:'?></h2>
<?php   echo $al->file('ccm-b-file', 'fID', 'Choose file',$bf);?>
<br />
<h2><?php   echo 'CSV delimiter:'?></h2>
<select name="delimiter" id="delimiter">
<option value=";" <?php   echo ($delimiter == ";"?"selected=\"selected\"":"")?>>semicolon</option>
<option value="," <?php   echo ($delimiter == ","?"selected=\"selected\"":"")?>>comma</option>
<option value="<?php   echo "\t";?>" <?php   echo (($delimiter == "\t") ? "selected=\"selected\"" : "");?>>tab</option>
<option value="|" <?php   echo ($delimiter == "|"?"selected=\"selected\"":"")?>>pipe</option>
</select>
<br />
<table>
<tr>
<td><input id="checking" type="checkbox" name="first_title" <?php   if ($first_title) echo 'checked="checked"'?> /></td>
<td><label for="checking">Treat the first row as column titles</label></td>
</tr>
</table>
<br />
<b>Lines per page (0=Unlimited):</b>
<input type="text" size="4" name="items_per_page" value="<?php  echo intval($items_per_page); ?>" /><br />



