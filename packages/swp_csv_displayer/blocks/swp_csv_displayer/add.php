<?php  
/*
Project: CSV Displayer block for concrete5 cms
Author: Anton Schepetkov
Group: Smart Web Projects
www.smartwebprojects.net
*/
defined('C5_EXECUTE') or die(_("Access Denied."));
$includeAssetLibrary = true;
$al = Loader::helper('concrete/asset_library');
?>
<h2><?php   echo 'CSV file:'?></h2>
<?php   echo $al->file('ccm-b-file', 'fID', 'Choose file');?>
<br />
<h2><?php   echo 'CSV delimiter:'?></h2>
<select name="delimiter" id="delimiter">
<option value=";">semicolon</option>
<option value=",">comma</option>
<option value="<?php   echo "\t"; ?>">tab</option>
<option value="|">pipe</option>
</select>
<br />
<table>
<tr>
<td><input id="checking" type="checkbox" name="first_title" /></td>
<td><label for="checking">Treat the first row as column titles</label></td>
</tr>
</table>

