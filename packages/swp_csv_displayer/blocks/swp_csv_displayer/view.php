<?php  
/*
Project: CSV Displayer block for concrete5 cms
Author: Anton Schepetkov
Group: Smart Web Projects
www.smartwebprojects.net
*/
 defined('C5_EXECUTE') or die(_("Access Denied.")); ?>

<div class="swp-csv-displayer">
<?php  
$f = File::getByID($fID);
$filename = $f->getPath();
$handle = fopen($filename, "r");
$is_first_row = true;

if ($handle) {
	echo "<table border=1>\n";

	if ($items_per_page > 0) {
		$page_param_id = "page_b".$bID;
		$current_page = intval($_GET[$page_param_id]);
		if ($current_page < 1)
			$current_page = 1;
		
		// counting total number of lines
		$total_lines = 0;
		while (($data = fgetcsv($handle, 1000, $delimiter)) !== FALSE) {
			$total_lines++;
		}
		fclose($handle);
		$handle = fopen($filename, "r");
	}

	if ($items_per_page > 0 && $current_page > 1) {
		// skip N rows
		$rows_to_skip = $items_per_page * ($current_page - 1);
		while ($rows_to_skip > 0 && (fgetcsv($handle, 1000, $delimiter) !== FALSE)) {
			$rows_to_skip--;
			continue;
		}
	}

	$current_row = 0;
	while (($data = fgetcsv($handle, 1000, $delimiter)) !== FALSE && ($items_per_page < 1 || $current_row < $items_per_page)) {
	        $num = count($data);
	        if ($first_title) {
	           	$m=0;
	        	if ($is_first_row) {
	    	   		 echo "<tr>";
		    	    for ($c=0; $c < $num; $c++) {
		        	    echo "<th>" . $data[$c] . "</th>";
		        	    $m++;
		       		}
	       			echo "</tr>\n";
	       			$is_first_row = false;
	       		}
	       		else {
		    	    echo "<tr>";
	    	    	for ($c=$m; $c < $num; $c++) {
	        	    echo "<td>" . $data[$c] . "</td>";
	        		 }
	       		echo "</tr>\n";
	       		}
	        }
	     	else {
	    		echo "<tr>";
	        	for ($c=0; $c < $num; $c++) {
	        	   echo "<td>" . $data[$c] . "</td>";
	       		 }
	        	echo "</tr>\n";
           }
		   $current_row++;
	}
	
	fclose($handle);
	echo "</table>";
	
	// pagination
	if ($items_per_page > 0) {
		$uh = Loader::helper("url");
		echo '<br />Pages: ';
		$total_pages = ceil($total_lines / $items_per_page);
		for ($i=1;$i<=$total_pages; $i++) {
			if ($current_page == $i) {
				echo ' <b>'.$i.'</b> ';
			} else {				
				echo ' <a href="'. $uh->setVariable($page_param_id, $i) .'">'.$i.'</a> ';
			}
		}
	}
}


?>
</div>