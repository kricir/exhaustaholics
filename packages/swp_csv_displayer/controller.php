<?php  
defined('C5_EXECUTE') or die(_("Access Denied."));

class SwpCsvDisplayerPackage extends Package {

	protected $pkgHandle = 'swp_csv_displayer';
	protected $appVersionRequired = '5.3.0';
	protected $pkgVersion = '1.1';

	public function getPackageDescription() {
		return t('Package for displaying html table from .csv file.<br />By <a href="http://www.smartwebprojects.net/">www.smartwebprojects.net</a>');
	}

	public function getPackageName() {
		return t('CSV displayer');
	}

	public function install() {
		$pkg = parent::install();

		Loader::model('block_types');

		// install block
		BlockType::installBlockTypeFromPackage('swp_csv_displayer', $pkg);

	}

}