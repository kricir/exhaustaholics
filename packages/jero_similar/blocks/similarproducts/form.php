<div class="ccm-block-field-group">
    <h2><?php  echo t('Product') ?></h2>
    <?php  echo $form->radio('inheritProductIDFromCurrentPage', '0', $inheritProductIDFromCurrentPage) ?>
    <?php  echo $form->label('inheritProductIDFromCurrentPage2', t('Choose specific product')) ?>
    &nbsp;&nbsp;
    <?php  echo $form->radio('inheritProductIDFromCurrentPage', '1', $inheritProductIDFromCurrentPage) ?>
    <?php  echo $form->label('inheritProductIDFromCurrentPage1', t('Inherit from current page')) ?>

    <div class="ccm-core-commerce-product-block-choose-product" style="<?php  if ($inheritProductIDFromCurrentPage == 1) { ?>display: none<?php  } ?>">
	<br/>
	<?php  echo $prh->selectOne('productID', t('Select Product'), $product); ?>
    </div>

</div>

<div class="ccm-block-field-group">
    <h2><?php  echo t('Product Search') ?></h2>
    <?php  echo $form->label('showWhat', t('Show')) ?>
    <?php  echo $form->radio('showWhat', 'S', $showWhat); ?>
    <?php  echo $form->label('showWhat', t('Similar products')) ?>
    &nbsp;&nbsp;
    <?php  echo $form->radio('showWhat', 'O', $showWhat); ?>
    <?php  echo $form->label('showWhat', t('Other products purchased')) ?><br/>
    &nbsp;&nbsp;
    <?php  echo $form->radio('showWhat', 'E', $showWhat); ?>
    <?php  echo $form->label('showWhat', t('Other products in set')) ?><br/>

    <?php  echo $form->label('productsToShow', t('Maximum number of products to show')) ?>
    <?php  echo $form->text('productsToShow', $productsToShow, array('size' => '3')) ?>
</div>

<div class="ccm-block-field-group">
    <h2><?php  echo t('Block title') ?></h2>
    <?php  echo $form->label('showBlockTitle', t('Show')) ?>
    <?php  echo $form->checkbox('showBlockTitle', 1, $showBlockTitle); ?>
    <div id="blockTitleDiv">
	<?php  echo $form->label('blockTitle', t('Title')) ?>&nbsp;&nbsp;&nbsp;
	<?php  echo $form->text('blockTitle', $blockTitle, array('size' => '50')) ?>
    </div>
</div>

<div class="ccm-block-field-group">
    <h2><?php  echo t('Inventory') ?></h2>
    <?php  echo $form->checkbox('hideSoldOut', 1, $hideSoldOut); ?>
    <?php  echo $form->label('hideSoldOut', t('Hide block if the product is sold out.')) ?>
    &nbsp;&nbsp;
    <?php  echo $form->checkbox('hideSimilarSoldOut', 1, $hideSimilarSoldOut); ?>
    <?php  echo $form->label('hideSimilarSoldOut', t('Exclude products if they are sold out.')) ?>
</div>
<div class="ccm-block-field-group">
    <h2><?php  echo t('Product Title') ?></h2>
    <?php  echo $form->radio('showTitle', 1, $showTitle); ?>
    <?php  echo $form->label('showTitle', t('Underneath')) ?>
    &nbsp;&nbsp;
    <?php  echo $form->radio('showTitle', 2, $showTitle); ?>
    <?php  echo $form->label('showTitle', t('On top')) ?>
    &nbsp;&nbsp;
    <?php  echo $form->radio('showTitle', 0, $showTitle); ?>
    <?php  echo $form->label('showTitle', t('No title')) ?>

    &nbsp;&nbsp;
    <?php  echo $form->checkbox('linkTitle', 1, $linkTitle); ?>
    <?php  echo $form->label('linkTitle', t('Link title to product page')) ?>
</div>

<div class="ccm-block-field-group">
    <h2><?php  echo t('Product Images') ?></h2>
    <div class="ccm-core-commerce-product-block-image-fields" style="padding-left:22px;">
	<table border="0" cellspacing="0" cellpadding="0">
	    <tr>
		<td valign="top"><?php  echo t('Image to display') ?><br/>
		    <?php  echo $form->select('primaryImage', $controller->getAvailableImageOptions(), $primaryImage); ?>
		</td>
		<td><div style="width:10px"></div></td>
		<td valign="top">
		    <?php  echo $form->label('imageMaxWidth', t('Max width')) ?><br/>
		    <?php  echo $form->text('imageMaxWidth', $imageMaxWidth, array('size' => '6')) ?>
		</td>
		<td><div style="width:10px"></div></td>
		<td valign="top">
		    <?php  echo $form->label('imageMaxHeight', t('Max height')) ?><br/>
		    <?php  echo $form->text('imageMaxHeight', $imageMaxHeight, array('size' => '6')) ?>
		</td>
		<td><div style="width:10px"></div></td>
		<td valign="top">
		    <?php  echo $form->label('linkImage', t('Link product image')) ?><br/>
		    <?php  echo $form->checkbox('linkImage', 1, $linkImage); ?>
		</td>
	    </tr>
	</table>
    </div>
</div>
