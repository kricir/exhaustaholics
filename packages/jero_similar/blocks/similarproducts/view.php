<?php     
defined('C5_EXECUTE') or die(_("Access Denied."));

if (is_object($controller->product)){
	if (!($controller->hideSoldOut && $controller->product->isSoldOut())) {
		if (count($controller->products)>0){
			$ih = Loader::helper('image');
?><div class="similarproducts"><?php     
			if ($controller->showBlockTitle) { ?><h3><?php      echo $controller->blockTitle ?></h3><?php      } 
			foreach ($controller->products as $sp){
				if ($product->getProductID() != $sp->getProductID()){
					switch ($controller->primaryImage){
						case 'prThumbnailImage':
							$image = $sp->getProductThumbnailImageObject();
							break;
						case 'prAltThumbnailImage':
							$image = $sp->getProductAlternateThumbnailImageObject();
							break;
						case 'prFullImage':
							$image = $sp->getProductFullImageObject();
							break;
					}
					if (is_object($image)) { 
						$r= $ih->getThumbnail($image,$controller->imageMaxWidth,$controller->imageMaxHeight);
					} else 
						$r=NULL;
					$pn=$sp->getProductName();
					$pid=$sp->getProductCollectionID();
					$el_array=array('controller'=>$controller,'pn'=>$pn);
					$linkImage=FALSE;
					if ($pid){
						$linksTo=Page::getById($pid);
						$purl=Loader::helper('navigation')->getLinkToCollection($linksTo);
						$el_array['purl']=$purl;
						$linkImage=TRUE;
					}
?>
<div class="similarproduct"><?php     
if ($controller->showTitle == 2) 
	$this->inc('elements/block_title.php',$el_array);
?>
<div class="similarproductimage" style="min-height:<?php      echo $controller->imageMaxHeight?>px"><?php     
if ($linkImage){
	?><a href="<?php      echo $purl ?>"><?php     
}
?><img alt="<?php      echo $pn ?>" title="<?php      echo $pn ?>" src="<?php      echo $r->src ?>" /><?php     
if ($linkImage){
	?></a><?php     
} ?>
</div><!--similarproductimage-->
<?php      if ($controller->showTitle == 1)
	$this->inc('elements/block_title.php',$el_array);
?>
</div><!--similarproduct-->
<?php     
				} // end if
			} // end foreach
?>
</div><!--similarproducts--><div class="spclear"></div>
<?php     
		} // end if count
	} // end hideSoldOut
} // end is_object
