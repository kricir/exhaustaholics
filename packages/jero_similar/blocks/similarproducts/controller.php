<?php 

defined('C5_EXECUTE') or die(_("Access Denied."));

class SimilarproductsBlockController extends BlockController {

    public function getBlockTypeDescription() {
	return t("Show similar products or other products purchased");
    }

    public function getBlockTypeName() {
	return t("Similar Products");
    }

    protected $btTable = 'btCoreCommerceSimilarProduct';
    protected $btInterfaceWidth = "500";
    protected $btInterfaceHeight = "550";

    public function on_start() {
	$this->set('prh', Loader::helper('form/product', 'core_commerce'));
	$this->addHeaderItem(Loader::helper('html')->javascript('ccm.core.commerce.search.js', 'core_commerce'));
	$this->addHeaderItem(Loader::helper('html')->css('ccm.core.commerce.search.css', 'core_commerce'));
    }

    public function view() {
	$db = Loader::db();
	$this->loadProduct();
	if (is_null($this->product))
	    return FALSE;
	if ($this->hideSoldOut) {
	    if ($this->product->isSoldOut())
		return FALSE;
	}
	switch ($this->showWhat) {
	    case 'S':
		$this->loadSimilarProducts();
		break;
	    case 'O':
		$this->loadProductsPurchased();
		break;
	    case 'E':
		$this->loadProductsFromSet();
		break;
	    default:
		die(t('Bad ShowWhat value'));
		break;
	}
    }

    public function add() {
	$this->set('imageMaxWidth', ECOMMERCE_PRODUCT_THUMBNAIL_WIDTH);
	$this->set('imageMaxHeight', ECOMMERCE_PRODUCT_THUMBNAIL_HEIGHT);
	$this->set('inheritProductIDFromCurrentPage', 1);
	$this->set('productsToShow', 6); /* I am not a number, I am a free man */
	$this->set('linkImage', 1);
	$this->set('linkTitle', 1);
	$this->set('showTitle', 1);
	$this->set('showBlockTitle', 1);
	$this->set('showWhat', 'S');
	$this->set('blockTitle', t('Similar products:'));
	$this->set('primaryImage', 'prThumbnailImage');
    }

    protected function loadProduct() {
	Loader::model('product/model', 'core_commerce');
	if ($this->productID) {
	    $product = CoreCommerceProduct::getByID($this->productID);
	} else if ($this->inheritProductIDFromCurrentPage) {
	    $c = Page::getCurrentPage();
	    $db = Loader::db();
	    $productID = $db->GetOne('select productID from CoreCommerceProducts where cID = ?', array($c->getCollectionID()));
	    if ($productID > 0) {
		$product = CoreCommerceProduct::getByID($productID);
	    }
	}

	if (is_object($product)) {
	    $this->product = $product;
	    $this->set('product', $product);
	}
    }

    public function loadProductsPurchased() {
	Loader::model('order/model', 'core_commerce');
	Loader::model('similar_products/list', 'jero_similar');
	$list = new JeroSimilarOrderList();
	$list->filter('oStatus', CoreCommerceOrder::STATUS_PENDING, '>'); // Exclude pending orders
	$list->filter('oStatus', CoreCommerceOrder::STATUS_CANCELLED, '<'); // Exclude cancelled orders
	$list->filter('ProductID', $this->product->productID, '='); //filter orders by product
	$list->setupSort('oDateAdded', 'desc'); /* Order by most recent first */
	$orders = $list->get(100, 0); /* Pick a sample size of 100 */
	$op = array();
	$oq = array();
	foreach ($orders as $o) {
	    foreach ($o->getProducts() as $p) {
		if ($this->hideSimilarSoldOut) {
		    if ($p->product->isSoldOut())
			continue;

		}
// Only show enabled products
		if (! $p->product->isProductEnabled())
		    continue;
		$oq[$p->productID]++;
		$op[$p->productID] = $p->product;
	    }
	}
	unset($oq[$this->product->productID]); // ignore our product
	arsort($oq); // sort by value (most popular), highest value first, maintaining indexes
	$oq = array_slice($oq, 0, $this->productsToShow, TRUE); // extract the first few products
	$prods = array();
	foreach ($oq as $k => $v) {
	    $prods[] = $op[$k];
	}
	$this->products = $prods;
    }

    private function loadProductsFromSet() {
	Loader::model('product/list', 'core_commerce');
	Loader::model('product/set', 'core_commerce');
	// Sadly there's no efficient way to get the set(s) a product might be in
	$db = Loader::db();
	$id = $this->product->getProductID();
	$sql = 'select prsID from CoreCommerceProductSetProducts where productID=?';
	$rows = $db->getAll($sql, array($id));
	if (count($rows)==0){
	    return false;
	}
	
	$list = new CoreCommerceProductList();
	// exclude the product we're dealing with, otherwise we're no better than the
	// product list block
	$list->filter('pr.productID', $id, '!=');
	// Only show enabled products
	$list->filter('pr.prStatus', 1, '=');

	foreach ($rows as $row) {
	    $list->filterBySet(CoreCommerceProductSet::getByID($row['prsID']));
	}
	$prods = $list->get($this->productsToShow, 0);
	$this->products = $prods;
    }

    private function loadSimilarProducts() {
	Loader::model('attribute/categories/core_commerce_product', 'core_commerce');
	$attributes = array('similar1', 'similar2', 'similar3');
	$prods = array();
	foreach ($attributes as $a) {
	    $sp = CoreCommerceProductAttributeKey::getByHandle($a);
	    if (($return = $this->getSimilarProducts($sp)) !== FALSE) {
		foreach ($return as $ret) {
		    if ($ret->productID != $this->product->productID)
			$prods[$ret->productID] = $ret;
		}
	    }
	}
	$this->products = $prods;
    }

    private function getSimilarProducts($AttKeyObj) {
	if (!is_object($AttKeyObj))
	    return FALSE;
	$av = $this->product->getAttributeValueObject($AttKeyObj);
	if (is_object($av)) {
	    $a = $av->getValue(); // we now have the attribute value
	    Loader::model('product/list', 'core_commerce');
	    $list = new CoreCommerceProductList();
	    $list->filterByAttribute($AttKeyObj->akHandle, $a);
// Only show enabled products
	    $list->filter('pr.prStatus', 1, '=');
	    if ($this->hideSimilarSoldOut)
		$list->displayOnlyProductsWithInventory();
	    $prods = $list->get($this->productsToShow + 1, 0);
	    if (count($prods) > 0) {
		return $prods;
	    }
	}
	return FALSE;
    }

    public function edit() {
	$this->loadProduct();
	$db = Loader::db();
    }

    public function getAvailableImageOptions() {
	$values = array(
	    'prFullImage' => t('Full Image'),
	    'prThumbnailImage' => t('Thumbnail Image'),
	    'prAltThumbnailImage' => t('Alternate Thumbnail')
	);
	return $values;
    }

    public function save($data) {
	$num = Loader::helper('validation/numbers');

	if (!$data['hideSoldOut']) {
	    $data['hideSoldOut'] = 0;
	}
	if (!$data['hideSimilarSoldOut']) {
	    $data['hideSimilarSoldOut'] = 0;
	}
	if (!$data['linkImage']) {
	    $data['linkImage'] = 0;
	}
	if (!$data['showWhat']) {
	    $data['showWhat'] = 'S';
	}
	if (!$data['linkTitle']) {
	    $data['linkTitle'] = 0;
	}
	if (!$data['showTitle']) {
	    $data['showTitle'] = 0;
	}
	if (!$data['showBlockTitle']) {
	    $data['showBlockTitle'] = 0;
	}

	if (!$data['productsToShow']) {
	    $data['productsToShow'] = 6; /* todo make this a db value */
	}

	if (!$data['inheritProductIDFromCurrentPage']) {
	    $data['inheritProductIDFromCurrentPage'] = 0;
	} else {
	    $data['inheritProductIDFromCurrentPage'] = 1;
	    $data['productID'] = 0;
	}

	if ($data['blockTitle'] == '') {
	    if ($data['showWhat'] == 'S')
		$data['blockTitle'] = t('Similar products:');
	    else
		$data['blockTitle'] = t('Other people bought:');
	}

	// validate the width/height of images for type
	$image_dimentions = array(
	    'imageMaxWidth', 'imageMaxHeight');
	foreach ($image_dimentions as $k) {
	    if (!$num->integer($data[$k])) {
		$data[$k] = 0; //set non-numeric values to 0
	    }
	}

	if ($data['imageMaxWidth'] == 0 || $data['imageMaxHeight'] == 0) {
	    $data['imageMaxWidth'] = ECOMMERCE_PRODUCT_THUMBNAIL_WIDTH;
	    $data['imageMaxHeight'] = ECOMMERCE_PRODUCT_THUMBNAIL_HEIGHT;
	}

	parent::save($data);
    }
}