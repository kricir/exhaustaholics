$(function() {
	$("input[name=inheritProductIDFromCurrentPage]").click(function() {
		if ($(this).val() == '1') {
			$('div[class=ccm-core-commerce-product-block-choose-product]').hide();
		} else {
			$('div[class=ccm-core-commerce-product-block-choose-product]').show();
		}
	});
	$("input[name=showBlockTitle]").change(function() {
		if ($(this).is(':checked'))
			$('#blockTitleDiv').show();
		else
			$('#blockTitleDiv').hide();
	});
	$("input[name=showWhat]").change(function() {
		var thus = $("input[name=blockTitle]");
		var title = thus.val();
		switch (title) {
			case 'Similar products:':
				thus.val('Other people bought:');
				break;
			case 'Other people bought:':
				thus.val('Similar products:');
				break;
		}
	});
	$("input[name=showBlockTitle]").trigger('change');
});
