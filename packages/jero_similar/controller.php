<?php          

defined('C5_EXECUTE') or die(_("Access Denied."));

class JeroSimilarPackage extends Package {

	protected $pkgHandle = 'jero_similar';
	protected $appVersionRequired = '5.4.1.1';
	protected $pkgVersion = '1.0.2';
	
	public function getPackageName() {
		return t("Similar Products"); 
	}	
	
	public function getPackageDescription() {
		return t("Show similar products or purchases");
	}
	
	public function install() {
		$installed = Package::getInstalledHandles();
		if (in_array('core_commerce',$installed)) {
			$ccpkg = Package::getByHandle('core_commerce');
			if(version_compare($ccpkg->getPackageVersion(), '2.0.0', '<')) {
				throw new Exception(t('You must upgrade the eCommerce add-on to version 2.0.0 or higher.'));
			}
		}
		else {
			throw new Exception(t('You must install eCommerce before installing this add-on.'));
		}

		$pkg = parent::install();

		Loader::model('attribute/categories/core_commerce_product', 'core_commerce');
		$similar1 = CoreCommerceProductAttributeKey::add('SELECT', array('akHandle' => 'similar1', 'akName' => t('Similar 1'), 'akIsSearchable' => false,'akIsSearchableIndexed'=>false,'akSelectAllowMultipleValues'=>false));
		$opt = new SelectAttributeTypeOption(0, 'Example 1', 1);
		$opt = $opt->saveOrCreate($similar1);
		$opt = new SelectAttributeTypeOption(0, 'Example 2', 2);
		$opt = $opt->saveOrCreate($similar1);
		$opt = new SelectAttributeTypeOption(0, 'Example 3', 2);
		$opt = $opt->saveOrCreate($similar1);

		$similar2 = CoreCommerceProductAttributeKey::add('SELECT', array('akHandle' => 'similar2', 'akName' => t('Similar 2'), 'akIsSearchable' => false,'akIsSearchableIndexed'=>false,'akSelectAllowMultipleValues'=>false));
		$opt = new SelectAttributeTypeOption(0, 'Example 1', 1);
		$opt = $opt->saveOrCreate($similar2);
		$opt = new SelectAttributeTypeOption(0, 'Example 2', 2);
		$opt = $opt->saveOrCreate($similar2);
		$opt = new SelectAttributeTypeOption(0, 'Example 3', 2);
		$opt = $opt->saveOrCreate($similar2);

		$similar3 = CoreCommerceProductAttributeKey::add('SELECT', array('akHandle' => 'similar3', 'akName' => t('Similar 3'), 'akIsSearchable' => false,'akIsSearchableIndexed'=>false,'akSelectAllowMultipleValues'=>false));
		$opt = new SelectAttributeTypeOption(0, 'Example 1', 1);
		$opt = $opt->saveOrCreate($similar3);
		$opt = new SelectAttributeTypeOption(0, 'Example 2', 2);
		$opt = $opt->saveOrCreate($similar3);
		$opt = new SelectAttributeTypeOption(0, 'Example 3', 2);
		$opt = $opt->saveOrCreate($similar3);
		
		// install block
		BlockType::installBlockTypeFromPackage('similarproducts', $pkg);		
	}

	public function uninstall() {
		parent::uninstall();
		Loader::model('attribute/categories/core_commerce_product', 'core_commerce');
		$ak=CoreCommerceProductAttributeKey::getByHandle('similar1');
		if (is_object($ak))
			$ak->delete();
		$ak=CoreCommerceProductAttributeKey::getByHandle('similar2');
		if (is_object($ak))
			$ak->delete();
		$ak=CoreCommerceProductAttributeKey::getByHandle('similar3');
		if (is_object($ak))
			$ak->delete();
	}
}
