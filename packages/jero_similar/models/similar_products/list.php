<?php     
defined('C5_EXECUTE') or die(_("Access Denied."));

Loader::model('order/list','core_commerce');

class JeroSimilarOrderList extends CoreCommerceOrderList {
	protected function createQuery(){
		parent::createQuery();
		$this->setupAttributeFilters("left join CoreCommerceOrderProducts on CoreCommerceOrderProducts.orderID=orders.orderID");
	}

	public function setupSort($sortby,$dir='desc') {
		$this->sortBy =  $sortby;
		$this->sortByDirection = $dir;
	}
}
