<?php      

defined('C5_EXECUTE') or die(_("Access Denied."));

class JacksHoneypotPackage extends Package {

	protected $pkgHandle = 'jacks_honeypot';
	protected $appVersionRequired = '5.5';
	protected $pkgVersion = '1.0';
	
	public function getPackageDescription() {
		return t("Adds Honeypot as a captcha option.");
	}
	
	public function getPackageName() {
		return t("Honeypot");
	}
	
	public function install() {
		$pkg = parent::install();
		Loader::model('system/captcha/library');
		SystemCaptchaLibrary::add('jacks_honeypot', t('Honeypot'), $pkg);		
	}
	
}