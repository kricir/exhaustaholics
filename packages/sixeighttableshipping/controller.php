<?php     

defined('C5_EXECUTE') or die(_("Access Denied."));

class SixeighttableshippingPackage extends Package {

	protected $pkgHandle = 'sixeighttableshipping';
	protected $appVersionRequired = '5.3.3.1';
	protected $pkgVersion = '1.1.0';
	
	public function getPackageDescription() {
		return t('Table-based shipping for the Concrete5 eCommerce package.');
	}
	
	public function getPackageName(){
		return t('Table-Based Shipping');
	}
	
	public function install() {
		$pkg = parent::install();
		$pkg->saveConfig('SHIPPING_TYPE_TABLE_VALUES', '0:0;');
		Loader::model('shipping/type', 'core_commerce');
		CoreCommerceShippingType::add('tablebased', t('Table-Based Shipping'), false, $pkg);
	}

	public function uninstall() {
		parent::uninstall();
		Loader::model('shipping/type', 'core_commerce');
		CoreCommerceShippingType::delete('tablebased');
	}
}
