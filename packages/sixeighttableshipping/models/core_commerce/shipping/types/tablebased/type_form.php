<?php    $form = Loader::helper('form'); ?>
<style type="text/css">
.table-shipping-row {
	margin-top:10px;
}
</style>
<table class="entry-form" cellspacing="1" cellpadding="0">
<tr>
	<td class="subheader"><?php   echo t('Based on')?></td>
	<td class="subheader"><?php   echo t('Values')?></td>
</tr>
<tr>
	<td valign="top" width="30%">
		<select name="SHIPPING_TYPE_TABLE_BASIS">
			<option value="Items" <?php   if($SHIPPING_TYPE_TABLE_BASIS=='Items'){echo 'selected="selected"'; } ?> > Number of Items</option>
			<option value="Price" <?php   if($SHIPPING_TYPE_TABLE_BASIS=='Price'){echo 'selected="selected"'; } ?> >Price</option>
			<option value="Weight" <?php   if($SHIPPING_TYPE_TABLE_BASIS=='Weight'){echo 'selected="selected"'; } ?> >Weight</option>
			<option value="ZipCode" <?php   if($SHIPPING_TYPE_TABLE_BASIS=='ZipCode'){echo 'selected="selected"'; } ?> >Zip Code</option>
		</select>
	</td>
	<td>
		<table border="0" cellspacing="1" cellpadding="0" id="table-shipping-values">
			<tr class="table-shipping-row-default">
				<td>Default Charge</td>
				<td><input type="text" size="5" name="SHIPPING_TYPE_TABLE_DEFAULT" value="<?php  echo $SHIPPING_TYPE_TABLE_DEFAULT ?>" /></td>
				<td colspan="2">&nbsp;</td>
			</tr>
			<?php  $firstRow = true; foreach($SHIPPING_TYPE_TABLE_VALUES as $row) { ?>
			<tr class="table-shipping-row">
				<td><span class="from-label">From</span></td>
				<td><input type="text" size="5" name="amount[]" value="<?php  echo $row['amount'] ?>" /></td>
				<td>Charge <input type="text" size="5" name="price[]" value="<?php   echo $row['price']; ?>" /></td>
				<td><a href="javascript:void(0);" onclick="$(this).parent().parent().remove()">Remove</a></td>
			</tr>
			<?php  } ?>
		</table>
		<table border="0" cellspacing="1" cellpadding="0">
			<tr><td colspan="4"><a class="add-row">Add Row</a></td></tr>
		</table>
	</td>
</tr>
</table>
<script type="text/javascript">
function addTableShippingRow() {
	$('#table-shipping-values').append('<tr class="table-shipping-row"><td><span class="from-label">From</span></td><td><input type="text" size="5" name="amount[]" /></td><td>Charge <input type="text" size="5" name="price[]" /></td><td><a href="javascript:void(0);" onclick="$(this).parent().parent().remove()">Remove</a></td></tr>');
}

function showDefault() {
	if ($('select[name="SHIPPING_TYPE_TABLE_BASIS"]').val() == 'ZipCode') {
		$('.table-shipping-row-default').show();
		$('.from-label').text('Zip Code');
		$('table table-shipping-row:first-child input[name="amount[]"]').attr('disabled');
	} else {
		$('.table-shipping-row-default').hide();
		$('.from-label').text('From');
		$('table table-shipping-row:first-child input[name="amount[]"]').attr('disabled', 'disabled').val(0);
	}
}

$(document).ready(function(){
	$('.add-row').click(function(){
		addTableShippingRow();
		showDefault();
	});

	$('select[name="SHIPPING_TYPE_TABLE_BASIS"]').change(function(){
		showDefault();
	});
	showDefault();
});
</script>
