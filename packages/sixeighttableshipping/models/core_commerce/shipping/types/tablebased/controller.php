<?php   
defined('C5_EXECUTE') or die(_("Access Denied."));
Loader::library('shipping/controller', 'core_commerce');
class CoreCommerceTablebasedShippingTypeController extends CoreCommerceShippingController {

	public function type_form() {
		$pkg = Package::getByHandle('core_commerce');
		$SHIPPING_TYPE_TABLE_DEFAULT = $pkg->config('SHIPPING_TYPE_TABLE_DEFAULT');
		$SHIPPING_TYPE_TABLE_BASIS = $pkg->config('SHIPPING_TYPE_TABLE_BASIS');
		$SHIPPING_TYPE_TABLE_VALUES = $this->getTableArrayFromString($pkg->config('SHIPPING_TYPE_TABLE_VALUES'));
		$this->set('SHIPPING_TYPE_TABLE_DEFAULT', $SHIPPING_TYPE_TABLE_DEFAULT);
		$this->set('SHIPPING_TYPE_TABLE_BASIS', $SHIPPING_TYPE_TABLE_BASIS);
		$this->set('SHIPPING_TYPE_TABLE_VALUES', $SHIPPING_TYPE_TABLE_VALUES);
	}
	
	public function validate() {
		$e = parent::validate();
		return $e;
	}
	
	public function save() {
		$pkg = Package::getByHandle('core_commerce');
		$pkg->saveConfig('SHIPPING_TYPE_TABLE_DEFAULT', $this->post('SHIPPING_TYPE_TABLE_DEFAULT'));
		$pkg->saveConfig('SHIPPING_TYPE_TABLE_BASIS', $this->post('SHIPPING_TYPE_TABLE_BASIS'));
		
		$tableValues = '';
		
		if(is_array($_POST['amount'])) {
			foreach($_POST['amount'] as $key => $amount) {
				$_POST['amount'][$key] = floatval($amount);
			}
			
			asort($_POST['amount']);
			foreach($_POST['amount'] as $key => $amount) {
				$tableValues .= $amount . ':' . floatval($_POST['price'][$key]) . ';';
			}
			
			$pkg->saveConfig('SHIPPING_TYPE_TABLE_VALUES', $tableValues);
		}
	}
	
	public function getAvailableShippingMethods($currentOrder) {
		Loader::model('cart', 'core_commerce');
		$cart = CoreCommerceCart::get();
		$pkg = Package::getByHandle('core_commerce');
		
		$default = $pkg->config('SHIPPING_TYPE_TABLE_DEFAULT');
		$basis = $pkg->config('SHIPPING_TYPE_TABLE_BASIS');
		$values = array_reverse($this->getTableArrayFromString($pkg->config('SHIPPING_TYPE_TABLE_VALUES')));
		
		if ($basis == 'ZipCode') {
			$totalPrice = $default;
			$address = $currentOrder->getAttribute('shipping_address');
			if (!$address) {
				$address = $currentOrder->getAttribute('billing_address');
			}
			if ($address && $address->getPostalCode()) {
				foreach ($values as $zipcode) {
					if ($zipcode['amount'] == $address->getPostalCode()) {
						$shipping = $zipcode['price'];
						break;
					}
				}
			}
		} else {
			$totalItems = 0;
			$totalWeight = 0;
			$totalPrice = $cart->getOrderTotal();;
		
			foreach($currentOrder->getProducts() as $pr) {
				$prQuantity = $pr->getQuantity();
				$totalItems += $prQuantity;
				$prObj = $pr->getProductObject();
				$totalWeight += $prObj->prWeight * $prQuantity;
			}
		
			switch($basis) {
				case 'Items':
					$shipping = $this->getShippingPriceFromItems($totalItems,$values);
					break;
				case 'Price':
					$shipping = $this->getShippingPriceFromItems($totalPrice,$values);
					break;
				case 'Weight':
					$shipping = $this->getShippingPriceFromItems($totalWeight,$values);
					break;
			}
		}

		$ecm = new CoreCommerceShippingMethod($this->getShippingType(), 'tablebased');
		$ecm->setPrice($shipping);
		$ecm->setName(t('Basic Shipping'));
		
		return $ecm;
	}
	
	public function getTableArrayFromString($tableValues) {
		$rows = explode(';',$tableValues);
		$tableArray = array();
		foreach($rows as $row) {
			if($row != '') {
				$values = explode(':',$row);
				$tableArray[] = array('amount'=>$values[0],'price'=>$values[1]);
			}
		}
		return $tableArray;
	}
	
	public function getShippingPriceFromItems($amount,$shippingTable) {
		foreach($shippingTable as $row) {
			if($amount >= $row['amount']) {
				return $row['price'];	
			}
		}
	}
	
}
