<?php      
defined('C5_EXECUTE') or die(_("Access Denied."));

class JeroSidebarCartPackage extends Package {
	protected $pkgHandle = 'jero_sidebar_cart';
	protected $appVersionRequired = '5.4.1.1';
	protected $pkgVersion = '0.9'; 
	
	public function getPackageName() {
		return t("Sidebar Cart"); 
	}	
	
	public function getPackageDescription() {
		return t("Show cart products in sidebar");
	}
	
	public function install() {
		$installed = Package::getInstalledHandles();
		if (in_array('core_commerce',$installed)) {
			$ccpkg = Package::getByHandle('core_commerce');
			if(version_compare($ccpkg->getPackageVersion(), '2.0.0', '<')) {
				throw new Exception(t('You must upgrade the eCommerce add-on to version 2.0.0 or higher.'));
			}
		}
		else {
			throw new Exception(t('You must install eCommerce before installing this add-on.'));
		}
		$pkg = parent::install();
		BlockType::installBlockTypeFromPackage('sidebarcart', $pkg);		
	}
}
