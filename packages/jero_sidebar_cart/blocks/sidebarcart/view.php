<?php 
defined('C5_EXECUTE') or die(_("Access Denied."));

if ($controller->hideIfEmpty){
	if ($controller->getQty()==0)
		return;
}

echo '<div id="SidebarCart">';
echo '<h3>'.$controller->blockTitle.'</h3>';

if ($controller->getQty()==0){
	echo '<p>'.$controller->emptyText.'</p></div>';
	return;
}

echo '<ul class="sidebarcart">';
foreach($controller->order->getProducts() as $p) {
	if (++$i>$controller->productsToShow){
		$more=$controller->getQty()-$controller->productsToShow;
		echo '<li>+'.$more.t(' more').'</li>';
		break;
	}
	echo '<li>';
	unset($url);
	if($controller->linkProduct){
		if ($url= $controller->getLink($p->product))
			echo '<a href="'.$url.'">';
	}
	echo $p->product->prName;
	if ($controller->showQty)
		echo ' ('.$p->quantity.')';
	if ($url)
		echo '</a>';
	echo '</li>';
}?>
</ul>
</div>
