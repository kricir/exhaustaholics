<?php  
defined('C5_EXECUTE') or die(_("Access Denied."));
class SidebarcartBlockController extends BlockController {
		
	public function getBlockTypeDescription() {
		return t("Show cart products in sidebar");
	}

	public function getBlockTypeName() {
		return t("Cart Contents");
	}

	protected $btTable = 'btSidebarCart';
	protected $btInterfaceWidth = "500";
	protected $btInterfaceHeight = "350";

	private $qty=0;

	public function view(){
		if (!array_key_exists('coreCommerceCurrentOrderID',$_SESSION))
			return;
		Loader::model('order/model', 'core_commerce');
		$order=CoreCommerceOrder::getByID($_SESSION['coreCommerceCurrentOrderID']);
		if (!is_object($order))
			return;
		$order->loadProducts();
		$this->qty=count($order->getProducts());
		$this->order=$order;
	}
			
	public function getQty(){
		return $this->qty;
	}

	public function getLink($product){
		$pid=$product->getProductCollectionID();
		if ($pid==0)
			return FALSE;
		$linksTo=Page::getById($pid);
		$url=Loader::helper('navigation')->getLinkToCollection($linksTo);
		return $url;
	}

	public function save($data) {
		$num = Loader::helper('validation/numbers');
		if (!$data['productsToShow']) {
			$data['productsToShow'] = 5;
		}
		if (!$num->integer($data['productsToShow'])) {
			 $data['productsToShow'] = 5;
		}
		if (!$data['hideIfEmpty']) {
			$data['hideIfEmpty'] = 0;
		}
		if (!$data['showQty']) {
			$data['showQty'] = 0;
		}
		if (!$data['linkProduct']) {
			$data['linkProduct'] = 0;
		}
		if ($data['emptyText']==''){
			$data['emptyText']=t('Your cart is empty');
		}
		parent::save($data);
	}

	public function add(){
		$this->set('productsToShow',5);
		$this->set('hideIfEmpty',0);
		$this->set('showQty',1);
		$this->set('linkProduct',1);
		$this->set('emptyText',t('Your cart is empty'));
		$this->set('blockTitle','Your cart');
	}
}
