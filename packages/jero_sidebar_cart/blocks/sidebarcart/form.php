<div class="ccm-block-field-group">
	<h2><?php  echo t('Title') ?></h2>
	<div id="blockTitleDiv">
		<?php  echo $form->label('blockTitle', t('Title'))?>&nbsp;&nbsp;&nbsp;
		<?php  echo $form->text('blockTitle', $blockTitle, array('size'=>'50'))?>
		<br/><br/>
		<?php  echo $form->label('emptyText', t('Empty text'))?>&nbsp;&nbsp;&nbsp;
		<?php  echo $form->text('emptyText', $emptyText, array('size'=>'50'))?><br/>

		<?php  echo $form->checkbox('hideIfEmpty',1,$hideIfEmpty); ?>
		<?php  echo $form->label('hideIfEmpty',t('Hide block if cart is empty'))?>
	</div>
</div>
<div class="ccm-block-field-group">
	<h2><?php  echo t('Products') ?></h2>
	<div id=productDiv">
		<?php  echo $form->label('productsToShow', t('Maximum number of products to show'))?>
                <?php  echo $form->text('productsToShow', $productsToShow, array('size'=>'3'))?><br/>
		<?php  echo $form->checkbox('linkProduct',1,$linkProduct); ?>
		<?php  echo $form->label('linkProduct',t('Link products to pages'))?><br/>
		<?php  echo $form->checkbox('showQty',1,$showQty); ?>
		<?php  echo $form->label('showQty',t('Show quantity'))?>
	</div>
</div>
