<?php   
defined('C5_EXECUTE') or die(_("Access Denied."));
$ih = Loader::helper('concrete/interface');
$al = Loader::helper('concrete/asset_library');
$form = Loader::helper('form');
$fh = Loader::helper('form/color');
Loader::element('editor_init');
$curSlides = $_REQUEST['slidecount'];
$nextSlide = $curSlides + 1;

?>

<div id="slide-<?php  echo $nextSlide;?>" class="slide" style="border-bottom:2px solid #cecece;">
	<div id="slidetitlebar-<?php  echo $nextSlide;?>" class="slidetitlebar" style="margin-bottom:10px;">
		<div style="float:left;">
			<!-- Title, Active, Delete, Order -->
			<div style="font-weight:bold;margin-top:7px;font-size:15px;"><?php  echo t('Slide');?> <?php  echo $nextSlide;?> <span id="slidesummary-<?php  echo $nextSlide;?>"></span></div>
		</div>
		<div style="float:right;">		
			<div style="float:right;">
				<?php  echo $ih->button(t('Delete'),'javascript:void(0)','right','btn danger', array('onClick'=>'toggleDeleteSlide('.$nextSlide.');','style'=>'margin-top:-1px;width:60px;text-align:center;','id'=>'deleteslidebutton-'.$nextSlide));?>
				<?php  echo $ih->button(t('Edit'),'javascript:void(0)','right','btn primary editslidebutton', array('onClick'=>'toggleEditSlide('.$nextSlide.');','style'=>'margin-right:10px;margin-top:-1px;width:60px;text-align:center;','id'=>'editslidebutton-'.$nextSlide));?>
			</div>
			<div style="float:right;">
				<?php  echo t('Active');?>&nbsp;<?php  echo $form->checkbox('slideActive-'.$nextSlide, 1, '');?> 
				&nbsp;&nbsp;
				<?php  echo t('Order');?>&nbsp;<?php  echo $form->text('slideOrder-'.$nextSlide, $nextSlide, array('style'=>'width:30px'));?> 
				&nbsp;&nbsp;
			</div>		
		</div>
		<div style="clear:both;"></div>		
	</div>
	<div id="slideoptions-<?php  echo $nextSlide;?>" class="slideoptions" style="display:none;">
		
		<div class="slide-left">
		
			<div class="clearfix">
				<label style="text-align:left;width:150px;">	
				<?php  echo t('Template');?>		
				</label>
				<div class="input">
					<?php  echo $form->select('slideTemplate-'.$nextSlide, array(''=>t('Please Select'),'imageleft'=>t('Image Left'),'imageright'=>t('Image Right'),'fullimage'=>t('Full Width Image'), 'fulltext'=>t('Full Width Text')), '', array('style'=>'width:150px;'));?>
					<span class="help-block"><?php  echo t('Choose a template for this slide');?></span>
				</div>
			</div>
	
			<div class="clearfix">
				<label style="text-align:left;">	
				<?php  echo t('Image');?>		
				</label>
				<div class="input">
					<?php  echo $al->image('slideImage-'.$nextSlide, 'slideImage-'.$nextSlide, t('Choose File')); ?>
					<span class="help-block"><?php  echo t('Choose an image for the slide');?></span>
				</div>
			</div>	
			
			<div class="clearfix">
				<label style="text-align:left;">	
				<?php  echo t('Image ALT text');?>		
				</label>
				<div class="input">
					<?php  echo $form->text('slideImageAltText-'.$nextSlide, '', array('style'=>'width:150px;'));?>
					<span class="help-block"><?php  echo t('Set the Alt text for the image');?></span>
				</div>
			</div>		
			
		</div>
		
		<div class="slide-right">	
				<?php  
				echo $form->textarea('slideText-'.$nextSlide, '', array('class' =>'ccm-advanced-editor'));
				?>
				<span class="help-block"><?php  echo t('Enter the text/html content for the slide');?></span>
		</div>
		<div style="clear:both;"></div>			
	</div>	
</div>