ccmValidateBlockForm = function() {
	
	// Formigo Slider Validation routines
	var sliderHeight = $("#sliderHeight").val();
	var sliderWidth = $("#sliderWidth").val();
	var sliderDisplayDuration = $("#sliderDisplayDuration").val();
	var sliderTransitionSpeed = $("#sliderTransitionSpeed").val();
	var sliderButtonStyle = $("#sliderButtonStyle").val();
	var sliderButtonColor = $("#sliderButtonColor").val();
	var sliderButtonHoverColor = $("#sliderButtonHoverColor").val();
	var sliderBackgroundColor = $("#sliderBackgroundColor").val();
	var slideCount = $("#slidecount").val();
	var isTemplateError = false;
	var isError = false;
	
	// Check sliderHeight
	if (sliderHeight == '') {
		
		ccm_addError(ccm_t('badHeight'));
		isError = true;
		
	}
	
	if (isNaN(sliderHeight)) {
		
		ccm_addError(ccm_t('badHeight'));
		isError = true;
		
	}
	
	// Check sliderWidth
	if (sliderWidth == '') {
		
		ccm_addError(ccm_t('badWidth'));
		isError = true;
	
	}
		
	if (isNaN(sliderWidth)) {
		
		ccm_addError(ccm_t('badWidth'));
		isError = true;
		
	}	
	
	// Check sliderDisplayDuration
	if (sliderDisplayDuration == '') {
		
		ccm_addError(ccm_t('badDuration'));
		isError = true;
	
	}
	
	if (isNaN(sliderDisplayDuration)) {
		
		ccm_addError(ccm_t('badDuration'));
		isError = true;
		
	}
	
	// Check sliderTransitionSpeed
	if (sliderTransitionSpeed == '') {
		
		ccm_addError(ccm_t('badTransition'));
		isError = true;
	
	}
	
	if (isNaN(sliderTransitionSpeed)) {
		
		ccm_addError(ccm_t('badTransition'));
		isError = true;
		
	}
	
	// Check sliderButtonStyle
	if (sliderButtonStyle == '') {
		
		ccm_addError(ccm_t('badButtonStyle'));
		isError = true;
	
	}
		
	// Check sliderButtonColor
	if (sliderButtonColor == '') {
		
		ccm_addError(ccm_t('badButtonColor'));
		isError = true;
	
	}
	
	// Check sliderButtonHoverColor
	if (sliderButtonHoverColor == '') {
		
		ccm_addError(ccm_t('badButtonHoverColor'));
		isError = true;
	
	}	
	
	// Check slides, minimum required input is template type.
	for($i=1;$i<=slideCount;$i++) {
		
		// Only checked enabled slides, if disabled they're for deletion
		if($('#slide-' + $i + ' :input').attr('disabled') != 'disabled') {
			
			// Only show one error, regardless of number slides with empty template
			if(!isTemplateError) {
				
				if ($("#slideTemplate-" + $i).val() == '') {
				
					ccm_addError(ccm_t('badSlideTemplate'));
					isError = true;
					isTemplateError = true;

				}
			}
			
		}	
	}
	
	// Need this to prevent tinyMCE instance removal if submit and validation fails	
	if(isError) {
		
		$("#errorstate").val('true');
		
	} else {
	
		$("#errorstate").val('false');
		
	}	
			
   return false;

}

/* ========================================================
 * bootstrap-tab.js v2.0.3
 * http://twitter.github.com/bootstrap/javascript.html#tabs
 * ========================================================
 * Copyright 2012 Twitter, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ======================================================== */


!function ($) {

  "use strict"; // jshint ;_;


 /* TAB CLASS DEFINITION
  * ==================== */

  var FormigoSliderTab = function ( element ) {
    this.element = $(element)
  }

  FormigoSliderTab.prototype = {

    constructor: FormigoSliderTab

  , show: function () {
      var $this = this.element
        , $ul = $this.closest('ul:not(.dropdown-menu)')
        , selector = $this.attr('data-target')
        , previous
        , $target
        , e

      if (!selector) {
        selector = $this.attr('href')
        selector = selector && selector.replace(/.*(?=#[^\s]*$)/, '') //strip for ie7
      }

      if ( $this.parent('li').hasClass('active') ) return

      previous = $ul.find('.active a').last()[0]

      e = $.Event('show', {
        relatedTarget: previous
      })

      $this.trigger(e)

      if (e.isDefaultPrevented()) return

      $target = $(selector)

      this.activate($this.parent('li'), $ul)
      this.activate($target, $target.parent(), function () {
        $this.trigger({
          type: 'shown'
        , relatedTarget: previous
        })
      })
    }

  , activate: function ( element, container, callback) {
      var $active = container.find('> .active')
        , transition = callback
            && $.support.transition
            && $active.hasClass('fade')

      function next() {
        $active
          .removeClass('active')
          .find('> .dropdown-menu > .active')
          .removeClass('active')

        element.addClass('active')

        if (transition) {
          element[0].offsetWidth // reflow for transition
          element.addClass('in')
        } else {
          element.removeClass('fade')
        }

        if ( element.parent('.dropdown-menu') ) {
          element.closest('li.dropdown').addClass('active')
        }

        callback && callback()
      }

      transition ?
        $active.one($.support.transition.end, next) :
        next()

      $active.removeClass('in')
    }
  }


 /* TAB PLUGIN DEFINITION
  * ===================== */

  $.fn.formigoSliderTab = function ( option ) {
    return this.each(function () {
      var $this = $(this)
        , data = $this.data('tab')
      if (!data) $this.data('tab', (data = new FormigoSliderTab(this)))
      if (typeof option == 'string') data[option]()
    })
  }

  $.fn.formigoSliderTab.Constructor = FormigoSliderTab

}(window.jQuery);