<?php   
	defined('C5_EXECUTE') or die("Access Denied.");
	class FormigoSliderBlockController extends BlockController {
	
		protected $btTable = 'btFormigoSlider';
		protected $btInterfaceWidth = "960";
		protected $btInterfaceHeight = "450";
		protected $btWrapperClass = 'ccm-ui';
		
		public $slides;
		private $searchableContent;
		
		public function getBlockTypeDescription() {
			
			return t("Adds a well featured Content Slider to your website");
			
		}
		
		public function getBlockTypeName() {
			
			return t("Formigo Slider");
			
		}
		
		public function getSearchableContent(){
			
			return $this->searchableContent;
			
		}
		
		public function on_start(){
			
			## Get our slides for this Block instance		
			$html = Loader::helper('html');	
			
			$curBlockID  = $this->bID;
		
			$db = loader::db();
			$sql= 'select * from btFormigoSliderSlides where slideBlockID = ? order by slideOrder;';
			$slides = $db->getAll($sql,array($curBlockID));
			
			$slideHtml = '';
			$slideCounter = 0;
			
			loader::library('slideBuilder','formigo_slider');
			$sb = new slideBuilder();
			
			if($slides) {
				
				foreach ($slides as $slide) {
					
					$slideImage = File::getByID($slide['slideImage']);
					$slideImageURI = $html->image($slideImage->getURL());
					
					$slides[$slideCounter]['slideImageURI'] = $slideImageURI;
					
					$slideCounter ++; 
					$slideHtml .= $sb->buildSlideHtml($slide, $slideCounter);

					## Index the text/html sections of ths slides
					$this->searchableContent .= $slide['slideText'].' ';
					
				}	
				
				$this->searchableContent = substr($this->searchableContent,0,-1);
				$this->slides = $slides;
			
			}
			
			$this->set('slides',$slideHtml);
			$this->set('slideCount', $slideCounter);
			
		}		
		
		public function on_page_view() {
			
			$html = Loader::helper('html');
			$this->addHeaderItem($html->javascript(BASE_URL.DIR_REL.'/packages/formigo_slider/js/formigoSlider.js'));
			
		}	
					
		public function view(){		
			
			## Send  $slides array to view
			$this->set('slides',$this->slides);
					
		}		
		
		public function save($args) {
			
			$curBlockID  = $this->bID;
			$slideCount = $args['slidecount'];
			
			$args['sliderAutoplay'] = isset($args['sliderAutoplay']) ? 1 : 0;
			$args['sliderSideButtonNav'] = isset($args['sliderSideButtonNav']) ? 1 : 0;
			
			$db = Loader::db();
			
			## Delete slides
			$db->execute('delete from btFormigoSliderSlides where slideBlockID = ?;',array($curBlockID));
			
			## Insert slides
			for($i=1;$i<=$slideCount;$i++) {
				
				if(isset($args['slideOrder-'.$i])) {
					
					$args['slideActive-'.$i] = isset($args['slideActive-'.$i]) ? 1 : 0;
				
					$sql = "insert into btFormigoSliderSlides (slideBlockID, slideTemplate, slideText, slideImage, slideImageAltText, slideOrder, slideActive) values (?, ?, ?, ?, ?, ?, ?);";
					$db->execute($sql,array($curBlockID,$args['slideTemplate-'.$i],$args['slideText-'.$i],$args['slideImage-'.$i],$args['slideImageAltText-'.$i],$args['slideOrder-'.$i],$args['slideActive-'.$i]));
			
				}
			}	
			
			parent::save($args);
			
		}
		
		public function getJavaScriptStrings() {
	      
			return array(
				
	         	'badHeight' => t('Please specify a numeric height for the slider'),
				'badWidth' => t('Please specify a numeric width for the slider'),
				'badDuration' => t('Please specify a numeric slide duration time'),
		        'badTransition' => t('Please specify a numeric slide transition speed'),
				'badButtonStyle' => t('Please choose a button style for the slider'),
				'badButtonColor' => t('Please choose a button color for the slider'),
				'badButtonHoverColor' => t('Please choose a button hover color for the slider'),
				'badSlideTemplate' => t('Please choose a template for each of your slides')
	      	
			);
	   	
		}
		
		public function duplicate($newID) {
		
			## Keep slides in sync (blockID changes when a custom template is later applied)
			$syncSlides = $this->syncSlides($newID, $this->bID);
			parent::duplicate($newID);
			
		}	
		
		private function syncSlides($newID,$curID) {
			
			$db = Loader::db();
			$db->execute('UPDATE btFormigoSliderSlides SET slideBlockID = ? WHERE slideBlockID = ?', array($newID, $curID));
								
		}

	}
?>