<?php   
defined('C5_EXECUTE') or die(_("Access Denied.")); 

## Restrict this to one instance per page
$pageBlocks = $page->getBlocks();
$sliderInUse = 0;
foreach ($pageBlocks as $pageBlock){

	if ($pageBlock->btHandle == 'formigo_slider') {

		$sliderInUse = 1;	
	
	} 
	
}

if(!$sliderInUse){

$ih = Loader::helper('concrete/interface');
$al = Loader::helper('concrete/asset_library');
$form = Loader::helper('form');
$fh = Loader::helper('form/color');
Loader::element('editor_init');
?>

<style type="text/css">
.ccm-ui .nav a:hover{color:#666666;}
.ccm-block-field-group{border:none !important;}
.slide-left{float:left;width:45%;margin-right:60px;}
.slide-right{float:left;width:47%;}
.slide{padding:10px;padding-right:0px;padding-bottom:0px;}
.slidetitlebar{padding-right:10px;}
.notes{margin-top:20px;}
.notes h5{color:#999999;}
</style>

<div class="ccm-block-field-group clearfix">
	
	<ul id="formigoslidertab" class="nav tabs">
		<li class="active"><a style="outline:none;" href="#settings" data-toggle="tab"><?php  echo t('Settings');?></a></li>
		<li><a style="outline:none;" href="#slides" data-toggle="tab"><?php  echo t('Slides');?></a></li>
	</ul>
	
	<div class="tab-content">
		
		<!-- Settings Start -->
		<div class="tab-pane active" id="settings">
			<div style="float:left;width:45%;">
				<h5 style="margin-bottom:20px;"><?php  echo t('Configure slider settings');?></h5>	
			
				<div class="clearfix">
					<label style="text-align:left;">	
					<?php  echo t('Height (px)');?>		
					</label>
					<div class="input">
						<?php  echo $form->text('sliderHeight', $sliderHeight, array('style'=>'width:100px;'));?>
						<span class="help-block"><?php  echo t('Set the height of the slider in pixels');?></span>
					</div>
				</div>
				
				<div class="clearfix">
					<label style="text-align:left;">	
					<?php  echo t('Width (px)');?>		
					</label>
					<div class="input">
						<?php  echo $form->text('sliderWidth', $sliderWidth, array('style'=>'width:100px;'));?>
						<span class="help-block"><?php  echo t('Set the width of the slider in pixels');?></span>
					</div>
				</div>
			
				<div class="clearfix">
					<label style="text-align:left;">	
					<?php  echo t('Display duration (seconds)');?>		
					</label>
					<div class="input">
						<?php  echo $form->text('sliderDisplayDuration', $sliderDisplayDuration, array('style'=>'width:100px;'));?>
						<span class="help-block"><?php  echo t('How long is seconds should each slide display for');?></span>
					</div>
				</div>
			
				<div class="clearfix">
					<label style="text-align:left;">	
					<?php  echo t('Transition Speed (miliseconds)');?>		
					</label>
					<div class="input">
						<?php  echo $form->text('sliderTransitionSpeed', $sliderTransitionSpeed, array('style'=>'width:100px;'));?>
						<span class="help-block"><?php  echo t('Set a speed in miliseconds for slide changes. 1000 equals 1 second');?></span>
					</div>
				</div>
				
				<div class="clearfix">
					<label style="text-align:left;">	
					<?php  echo t('Background Color');?>		
					</label>
					<div class="input">
						<?php  echo $fh->output('sliderBackgroundColor', '', $sliderBackgroundColor); ?>
						<span class="help-block" style="padding-top:10px;"><?php  echo t('Select a background color for the slider');?></span>
					</div>
				</div>	
								
			</div>
			<div style="float:right;width:49%;">
				
				<h5 style="margin-bottom:20px;"><?php  echo t('Customise your slider buttons');?></h5>

				<div class="clearfix">
					<label style="text-align:left;width:150px;">	
					<?php  echo t('Navigation Style');?>		
					</label>
					<div class="input">
						<?php  echo $form->select('sliderButtonStyle', array(''=>t('Please Select'),'1'=>t('Circular'), '2'=>t('Square')), $sliderButtonStyle, array('style'=>'width:150px;'));?>
						<span class="help-block"><?php  echo t('Choose a button style for the slider');?></span>
					</div>
				</div>

				<div class="clearfix">
					<label style="text-align:left;">	
					<?php  echo t('Color');?>		
					</label>
					<div class="input" style="padding-bottom:20px;">
						<?php  echo $fh->output('sliderButtonColor', '', $sliderButtonColor); ?>
						<span class="help-block" style="padding-top:10px;"><?php  echo t('Select a button color for the slider');?></span>
					</div>
				</div>
				
				<div class="clearfix">
					<label style="text-align:left;">	
					<?php  echo t('Selected/Hover Color');?>		
					</label>
					<div class="input" style="padding-bottom:20px;">
						<?php  echo $fh->output('sliderButtonHoverColor', '', $sliderButtonHoverColor); ?>
						<span class="help-block" style="padding-top:10px;"><?php  echo t('Select a button selected/hover color for the slider');?></span>
					</div>
				</div>
				
				<div class="clearfix">
					<div class="input">
						<?php  echo $form->checkbox('sliderSideButtonNav', 1, $sliderSideButtonNav);?> <?php  echo t('Enable Side Navigation');?>
						<span class="help-block"><?php  echo t('Enables additional navigation to left and right of slides');?></span>
					</div>
				</div>
				
				<div class="clearfix">
					<div class="input">
						<?php  echo $form->checkbox('sliderAutoplay', 1, $sliderAutoplay);?> <?php  echo t('Autoplay slideshow');?>

					</div>
				</div>
				
			</div>	
			<div style="clear:both"></div>
		</div>	
		<!-- Settings End -->
		
		<!-- Slides Start -->
		<div class="tab-pane" id="slides">
			
			<div id="headingarea" style="margin-bottom:5px;">	
				
				<?php  
				echo $ih->button(t('Hide All'),'javascript:void(0)','right','btn primary', array('onClick'=>'hideAll();','style'=>'margin-right:10px;width:60px;text-align:center;'));
				?>
				
				<?php  
				echo $ih->button(t('Add Slide'),'javascript:void(0)','right','btn success', array('onClick'=>'addNewSlide();','style'=>'margin-right:10px;width:60px;text-align:center;'));
				?>
				
				<h5><?php  echo t('Add and edit your slides');?></h5>
			</div>
			
			<div id="slidearea">
				<input type="hidden" name="slidecount" id="slidecount" value="1" />
				<input type="hidden" name="errorstate" id="errorstate" value="false" />
				
				<div id="slide-1" class="slide" style="border-bottom:2px solid #cecece;">
					<div id="slidetitlebar-1" class="slidetitlebar" style="margin-bottom:10px;">
						<div style="float:left;">
							<!-- Title, Active, Delete, Order -->
							<div style="font-weight:bold;margin-top:7px;font-size:15px;"><?php  echo t('Slide 1');?> <span id="slidesummary-1"></span></div>
						</div>
						<div style="float:right;">
							<?php  echo $ih->button(t('Delete'),'javascript:void(0)','right','btn danger', array('onClick'=>'toggleDeleteSlide(1);','style'=>'margin-top:-1px;width:60px;text-align:center;','id'=>'deleteslidebutton-1'));?>
							<?php  echo $ih->button(t('Edit'),'javascript:void(0)','right','btn primary editslidebutton', array('onClick'=>'toggleEditSlide(1);','style'=>'margin-right:10px;margin-top:-1px;width:60px;text-align:center;','id'=>'editslidebutton-1','class'=>'editslidebutton'));?>
						</div>						
						<div style="float:right;">
							
							<?php  echo t('Active');?>&nbsp;<?php  echo $form->checkbox('slideActive-1', 1, '');?> 
							&nbsp;&nbsp;
							<?php  echo t('Order');?>&nbsp;<?php  echo $form->text('slideOrder-1', '1', array('style'=>'width:30px'));?> 
							&nbsp;&nbsp;
						</div>
						<div style="clear:both;"></div>		
					</div>
					<div id="slideoptions-1" class="slideoptions" style="display:none;">
						
						<div class="slide-left">
						
							<div class="clearfix">
								<label style="text-align:left;width:150px;">	
								<?php  echo t('Template');?>		
								</label>
								<div class="input">
									<?php  echo $form->select('slideTemplate-1', array(''=>t('Please Select'),'imageleft'=>t('Image Left'),'imageright'=>t('Image Right'),'fullimage'=>t('Full Width Image'), 'fulltext'=>t('Full Width Text')), '', array('style'=>'width:150px;'));?>
									<span class="help-block"><?php  echo t('Choose a template for this slide');?></span>
								</div>
							</div>
					
							<div class="clearfix">
								<label style="text-align:left;">	
								<?php  echo t('Image');?>		
								</label>
								<div class="input">
									<?php  echo $al->image('slideImage-1', 'slideImage-1', t('Choose File')); ?>
									<span class="help-block"><?php  echo t('Choose an image for the slide');?></span>
								</div>
							</div>	
							
							<div class="clearfix">
								<label style="text-align:left;">	
								<?php  echo t('Image ALT text');?>		
								</label>
								<div class="input">
									<?php  echo $form->text('slideImageAltText-1', '', array('style'=>'width:150px;'));?>
									<span class="help-block"><?php  echo t('Set the Alt text for the image');?></span>
								</div>
							</div>		
							
						</div>
						
						<div class="slide-right">	
								<?php  
								echo $form->textarea('slideText-1', '', array('class' =>'ccm-advanced-editor'));
								?>
								<span class="help-block"><?php  echo t('Enter the text/html content for the slide');?></span>
						</div>
						<div style="clear:both;"></div>			
					</div>	
				</div>
				
			</div>
			
			<div class="notes" > 
				<h5><?php  echo t('Notes');?></h5>
				<ul>
					<li><?php  echo t('Slides will only display "h2" text at smartphone viewport size.');?></li>
					<li><?php  echo t('Images should be of consistent dimensions.');?></li>
					<li><?php  echo t('Fullwidth Image slides should use images sized the same as height and width of slider.');?></li>
					<li><?php  echo t('Image Left/Right slides should use images sized at the height of slider and half the width of slider.');?></li>
				</ul>	
			</div>
					
		</div>
		<!-- Slides End -->

	</div>
	
</div>	

<script type="text/javascript">

	$('#formigoslidertab a').click(function (e) {
		e.preventDefault();
		$(this).formigoSliderTab('show');
	})
	
	var toggleEditSlide = function(slide){
		
		if($('#editslidebutton-' + slide).text() == 'Edit') {
		
			$('#slideoptions-' + slide).slideDown();
			$('#editslidebutton-' + slide).text('Hide');
		
		} else {
			
			var content =  tinyMCE.get('slideText-' + slide).getContent();
			
			content = content.substring(0,50);
			$('#slideoptions-' + slide).slideUp();
			$('#editslidebutton-' + slide).text('Edit');
			
			if(content != ''){
			
				$('#slidesummary-' + slide).text(' : ' + stripHtml(content)+'...');
			
			}
			
		}	
		
	}
	
	var hideAll = function() {
		
		$('.slideoptions').slideUp();
	
		$('.editslidebutton').each(function(){

			$(this).text('Edit');
			
		});		
		
	}	

	var stripHtml = function (html){
		
	   var tmp = document.createElement("DIV");
	   tmp.innerHTML = html;
	   return tmp.textContent||tmp.innerText;
	
	}
		
	var toggleDeleteSlide = function (slide) {
		
		if($('#deleteslidebutton-' + slide).text() == 'Delete') {
			
			$('#slideoptions-' + slide).slideUp();
			$('#editslidebutton-' + slide).text('Edit');
			$('#slide-' + slide).css({background:'#fcdddd'});
			$('#editslidebutton-' + slide).css({visibility:'hidden'});
			$('#deleteslidebutton-' + slide).text('Undelete');
			$('#deleteslidebutton-' + slide).addClass('primary');
			$('#deleteslidebutton-' + slide).removeClass('danger');
			$('#slide-' + slide + ' :input').attr({disabled:'disabled'});
		
		} else {
			
			$('#deleteslidebutton-' + slide).text('Delete');
			$('#deleteslidebutton-' + slide).addClass('danger');
			$('#deleteslidebutton-' + slide).removeClass('primary');
			$('#editslidebutton-' + slide).css({visibility:'visible'});
			$('#slide-' + slide).css({background:'none'});
			$('#slide-' + slide + ' :input').removeAttr('disabled');
			
		}	
		
	}	
	
	var addNewSlide = function () {
		
		<?php  $uh = Loader::helper('concrete/urls'); ?>
		tinyMCE.triggerSave();
		$.ajax({

		  	url: "<?php  echo $uh->getToolsURL('createSlide','formigo_slider');?>?slidecount=" + escape($("#slidecount").val()),
			data:'',
		  	success: function(response) {

				$("#slidearea").append(response);
				curSlide = $("#slidecount").val();
	
				newSlide = parseInt(curSlide) + 1;
				$("#slidecount").val(newSlide);
				
				//Initialise new tinyMCE
				tinyMCE.execCommand('mceAddControl', false, 'slideText-' + newSlide);
					
			}

		});
		
	}	
	
	var initTinyMCE = function () {

		// Initialise tinyMCE, create tinyMCE instances
		tinyMCE.init({
			
			mode : "none",
			width: "300px", 
			height: "100x", 	
			inlinepopups_skin : "concreteMCE",
			theme_concrete_buttons2_add : "spellchecker",
			relative_urls : false,
			document_base_url: '<?php  echo BASE_URL . DIR_REL?>/',
			convert_urls: false,
			plugins: "inlinepopups,spellchecker,safari,advimage,advlink,table,advhr,xhtmlxtras,emotions,insertdatetime,paste,visualchars,nonbreaking,pagebreak,style",
			editor_selector : "<?php  echo $editor_selector?>",
			theme : "advanced",
			theme_advanced_buttons1 : "cut,copy,paste,pastetext,pasteword,|,undo,redo,|,formatselect,fontsizeselect,fontselect",
			theme_advanced_buttons2 : "bold,italic,underline,|,justifyleft,justifycenter,justifyright,justifyfull,|,bullist,numlist,|,link,unlink,anchor,|,forecolor,backcolor,|,image,code",
			theme_advanced_buttons3 : "",
			//theme_advanced_blockformats : "p,address,pre,h1,h2,h3,div,blockquote,cite",
			theme_advanced_fonts : "Andale Mono=andale mono,times;Arial=arial,helvetica,sans-serif;Arial Black=arial black,avant garde;Book Antiqua=book antiqua,palatino;Comic Sans MS=comic sans ms,sans-serif;Courier New=courier new,courier;Georgia=georgia,palatino;Helvetica=helvetica;Impact=impact,chicago;Symbol=symbol;Tahoma=tahoma,arial,helvetica,sans-serif;Terminal=terminal,monaco;Times New Roman=times new roman,times;Trebuchet MS=trebuchet ms,geneva;Verdana=verdana,geneva;Webdings=webdings;Wingdings=wingdings,zapf dingbats",
			theme_advanced_font_sizes : "1,2,3,4,5,6,7",
			theme_advanced_more_colors : 1,						
			theme_advanced_toolbar_location : "top",
			//theme_advanced_styles: "Note=ccm-note",		
			theme_advanced_toolbar_align : "left",
			spellchecker_languages : "+English=en"
			
		});
		
		tinyMCE.execCommand('mceAddControl', false, 'slideText-1');
		
	}	

	initTinyMCE();	
	
	$('form').submit(function(){
		
		// If no validation errors and block will save, remove our tinyMCE instances, or we can't edit the block again without page refresh
		if($("#errorstate").val() == 'false') {
			
			curSlide = $("#slidecount").val();
			for($i=1;$i<=curSlide;$i++) {
			
				tinymce.execCommand('mceRemoveControl',true,'slideText-' + $i);
			
			}
		}	
		
	})
	
	$(".btn:contains('Cancel')").click(function(){
		
		// Block cancel with no save, remove our tinyMCE instances, same reason as above
		curSlide = $("#slidecount").val();
		for($i=1;$i<=curSlide;$i++) {
		
			tinymce.execCommand('mceRemoveControl',true,'slideText-' + $i);
		
		}
		
	})
	
	$(".ui-dialog-titlebar-close").click(function(){
		
		// Block closed with no cancel or save, remove our tinyMCE instances, same reason as above
		curSlide = $("#slidecount").val();
		for($i=1;$i<=curSlide;$i++) {
		
			tinymce.execCommand('mceRemoveControl',true,'slideText-' + $i);
		
		}
		
	})
		
</script>

<?php 

} else {
	?>
	<div style="width:400px;margin-left:auto;margin-right:auto;margin-top:150px;">
	<h2><?php  echo t('Ooops..');?></h2>
	<p><?php  echo t('It looks like you\'re trying to add another Formigo Slider block to this page. Unfortunately, the responsive nature of this add-on means it\'s limited to one use per page.');?><p>
	</div>
	
	<script type="text/javascript">

		$('.ccm-button-right').hide();

	</script>
	
	<?php 	
}	
?>