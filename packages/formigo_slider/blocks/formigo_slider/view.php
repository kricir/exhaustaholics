<?php  defined('C5_EXECUTE') or die(_("Access Denied.")); ?>
<!-- Formigo Slider: Default template -->
<style type="text/css">
.formigo-slider-wrapper {
	position:relative; width:<?php  echo $sliderWidth;?>px; height:<?php  echo $sliderHeight;?>; overflow:hidden;	clear: both;
}
.formigo-slider {
	height: <?php  echo $sliderHeight;?>px; width: 100000px;
}
.formigo-slider-bg {
	background-color: <?php  echo $sliderBackgroundColor;?>;
}	
.formigo-slider-item {
	position:relative; float:left; width:<?php  echo $sliderWidth;?>px; height:<?php  echo $sliderHeight;?>; overflow:hidden;	
}
.formigo-slider-bottom-nav {
	position:relative;height:30px; width:100px; margin-left:auto; margin-right:auto; text-align:center; margin-top:20px;
}
<?php  if ($sliderButtonStyle == 1) {	
	$borderRadius = 50;	
} else {	
	$borderRadius = 0;	
}	
?>				
.formigo-slider-bottom-nav a {
	background-color: <?php  echo $sliderButtonColor;?>; border-radius: <?php  echo $borderRadius;?>px; width: 20px; height: 20px;
  	float: left; width: 10px;height: 10px;margin: 0 2px; 
}
.formigo-slider-bottom-nav a:hover {
	background-color: <?php  echo $sliderButtonHoverColor;?>; 
}
a.formigo-slider-select {
  	cursor: default !important; background-color: <?php  echo $sliderButtonHoverColor;?>; opacity:1.0;
}
a.formigo-slider-select:hover {
  	cursor: default !important;  background-color: <?php  echo $sliderButtonHoverColor;?>; opacity:1.0;
}
.formigo-slider-side-nav-next a {
	position: absolute; right: 0; width:40px; height:40px; margin: 0; padding: 0; text-indent: -999999px;display: none;
}
.formigo-slider-side-nav-previous a {
	position: absolute;left: 0; width:40px; height:40px; margin: 0; padding: 0; text-indent: -999999px;display: none;
}
</style>

<div class="formigo-slider-wrapper">

	<div class="formigo-slider-bg">
	
		<div class="formigo-slider">
			
			<?php  
	
			$formigoSliderBottomNav = '';
			$slideCount = 0;	
	
			foreach ($slides as $slide) {   
				
				if($slide['slideActive'] == 1) {
				
					$formigoSliderOutput = '<div class="formigo-slider-item">';
					$halfWidthImg = (int)$sliderWidth * 1/2;
					$halfWidthText = ((int)$sliderWidth * 1/2) - 70;
					## Output based on selected slide template
					switch ($slide['slideTemplate']) {
						
						case "imageleft":
					
						   	$formigoSliderOutput .= '<div style="width:'.$sliderWidth.'px;height:'.$sliderHeight.'px;" class="formigo-slider-item-image-left">';
							$formigoSliderOutput .= '<div style="width:'.$halfWidthImg.'px;height:'.$sliderHeight.'px;float:left;" class="formigo-slider-item-image-left-image">'.$slide['slideImageURI'].'</div>';
							$formigoSliderOutput .= '<div style="width:'.$halfWidthText.'px;float:right;" class="formigo-slider-item-image-left-text">'.$slide['slideText'].'</div>';
							$formigoSliderOutput .= '<div style="clear:both"></div></div>';
					        break;
					
						case "imageright":
					
						    $formigoSliderOutput .= '<div style="width:'.$sliderWidth.'px;height:'.$sliderHeight.'px;" class="formigo-slider-item-image-right">';
							$formigoSliderOutput .= '<div style="width:'.$halfWidthImg.'px;height:'.$sliderHeight.'px;float:right;" class="formigo-slider-item-image-right-image">'.$slide['slideImageURI'].'</div>';
							$formigoSliderOutput .= '<div style="width:'.$halfWidthText.'px;float:left;" class="formigo-slider-item-image-right-text">'.$slide['slideText'].'</div>';
							$formigoSliderOutput .= '<div style="clear:both"></div></div>';
					        break;
					
						case "fullimage":
					
						    $formigoSliderOutput .= '<div style="width:'.$sliderWidth.'px;" class="formigo-slider-item-full-image">'.$slide['slideImageURI'].'</div>';
					        break;
					
						case "fulltext":
					
						    $formigoSliderOutput .= '<div style="width:'.$sliderWidth.'px;" class="formigo-slider-item-full-text">'.$slide['slideText'].'</div>';
						    break;
		
					}	
							
					$formigoSliderOutput .= '</div>';
	
					$slideCount = $slideCount + 1;
				
					if($slideCount == 1){
	
						$formigoSliderBottomNav .= '<a href="#" id="formigo-slider-select-' .$slideCount. '" class="formigo-slider-select"></a>';	
	
					} else {
	
						$formigoSliderBottomNav .= '<a href="#" id="formigo-slider-select-' .$slideCount. '"></a>';
	
					}	
	
					echo $formigoSliderOutput;
				
				}	
				
			}
			
			$formigoSliderNavWidth = $slideCount * 14;	
			
			?>
		
		<?php  
		if ($sliderSideButtonNav == 1) {
			?>
			<!-- Side Side Nav Buttons --> 
			<div class="formigo-slider-side-nav-next"><a href="#"><?php  echo t('Next');?></a></div>
			<div class="formigo-slider-side-nav-previous"><a href="#"><?php  echo t('Prev');?></a></div>
			<!-- End Side Nav buttons -->
		<?php  
			}	
		?>
		
		<input type="hidden" name="curindex" id="curindex" value=0 />	
			
		</div><!-- END .formigo-slider -->
		
	</div><!-- END .formigo-slider-bg -->
	
	<div class="formigo-slider-bottom-nav" style="width:<?php  echo $formigoSliderNavWidth;?>px">
		
		<?php  echo $formigoSliderBottomNav;?>
		
	</div>	
		
</div><!-- END .formigo-slider-wrapper -->	

						
<script type="text/javascript">
/* Formigo Slider */	
var formigoSliderOptions = {
	
	width: <?php  echo $sliderWidth;?>,
	height: <?php  echo $sliderHeight;?>,
	delay: <?php  echo $sliderDisplayDuration;?>,
	transitionSpeed: <?php  echo $sliderTransitionSpeed;?>,
	autoplay:<?php  echo $sliderAutoplay;?>

};
</script>