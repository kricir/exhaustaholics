$(document).ready(function(){
	
	var formigoSlider = (new function() {
		
		var sliderWidth = parseInt(formigoSliderOptions.width);
		var sliderHeight = parseInt(formigoSliderOptions.height);
		var delay = parseInt(formigoSliderOptions.delay) * 1000, delayId;
		var transitionSpeed = parseInt(formigoSliderOptions.transitionSpeed);
		var autoplay = formigoSliderOptions.autoplay;	

		this.testSliderWidth = function() {
			
			var sliderWidthTemp = sliderWidth;
			// Function to ensure setup is correct based on type of slides in use and viewport
			$(".formigo-slider-wrapper").css({'width':'auto'});
			var containerWidth = $(".formigo-slider-wrapper").width();	
	
			if (containerWidth < sliderWidthTemp) {
				sliderWidthTemp = containerWidth;
			}	
		
			var halfWidthImg = sliderWidthTemp/2;
			var halfWidthText = sliderWidthTemp/2-70;

			// Width Resets
			$(".formigo-slider-wrapper").css({'width':sliderWidthTemp});
			$(".formigo-slider-item").css({'width':sliderWidthTemp});
	
			$(".formigo-slider-item-image-left").css({'width':sliderWidthTemp});
			$(".formigo-slider-item-image-left-image").css({'width':halfWidthImg});
			$(".formigo-slider-item-image-left-text").css({'width':halfWidthText});
	
			$(".formigo-slider-item-image-right").css({'width':sliderWidthTemp});
			$(".formigo-slider-item-image-right-image").css({'width':halfWidthImg});
			$(".formigo-slider-item-image-right-text").css({'width':halfWidthText});
	
			$(".formigo-slider-item-full-image").css({'width':sliderWidthTemp});
	
			$(".formigo-slider-item-full-text").css({'width':sliderWidthTemp});
		
			// Height Resets
			var imgHeight = null;

			if($(".formigo-slider-item-image-left-image > img")[0] || $(".formigo-slider-item-image-right-image > img")[0] || $(".formigo-slider-item-full-image > img")[0] ){
			
				// Establish relative height of image
				if (imgHeight == null) {imgHeight = $(".formigo-slider-item-image-left-image > img").height()};
				if (imgHeight == null) {imgHeight = $(".formigo-slider-item-image-right-image > img").height()};
				if (imgHeight == null) {imgHeight = $(".formigo-slider-item-full-image > img").height()};

				// Reset Slider
				$(".formigo-slider").css({'height':imgHeight});
				$(".formigo-slider-item").css({'height':imgHeight});
			
				$(".formigo-slider-item-image-left").css({'height':imgHeight});
				$(".formigo-slider-item-image-left-image").css({'height':imgHeight});
				
				$(".formigo-slider-item-image-right").css({'height':imgHeight});
				$(".formigo-slider-item-image-right-image").css({'height':imgHeight});

				$(".formigo-slider-item-full-image").css({'height':imgHeight});

				// Reset Buttons		
				$('.formigo-slider-side-nav-next a').css({
					top: ($('.formigo-slider-item').outerHeight())/2-20
				});
				$('.formigo-slider-side-nav-previous a').css({
					top: ($('.formigo-slider-item').outerHeight())/2-20
				});
			
				// Reset Text
				$('.formigo-slider-item-image-left-text').css({
					top: ($('.formigo-slider-item').outerHeight() - $('.formigo-slider-item-image-left-text').outerHeight())/2
				});		
				$('.formigo-slider-item-image-right-text').css({
					top: ($('.formigo-slider-item').outerHeight() - $('.formigo-slider-item-image-right-text').outerHeight())/2
				});
				$('.formigo-slider-item-full-text').css({
					top: ($('.formigo-slider').outerHeight() - $('.formigo-slider-item-full-text').outerHeight())/2
				});			
		
			} else {
			
				// Handle (unlikely) scenario where slides are full width text only
				// Establish relative height
				var containerHeight = (sliderWidthTemp/sliderWidth) * sliderHeight;	
				$(".formigo-slider-item").css({'height':containerHeight});
				$(".formigo-slider").css({'height':containerHeight});
	
				// Reset Buttons
				$('.formigo-slider-side-nav-next a').css({
					top: ($('.formigo-slider-item').outerHeight())/2-20
				});
				$('.formigo-slider-side-nav-previous a').css({
					top: ($('.formigo-slider-item').outerHeight())/2-20
				});
		
				// Reset Text 
				$('.formigo-slider-item-full-text').css({
					top: ($('.formigo-slider').outerHeight() - $('.formigo-slider-item-full-text').outerHeight())/2
				}); 
			
			}	 	
		
			return sliderWidthTemp;
		
		}	
		
		// Listener functions
		// Next slide	
		$('.formigo-slider-side-nav-next a').click(function(event) {

			event.preventDefault();
			var nextSlideWidth = $(".formigo-slider-wrapper").width();
			var nextIndex = parseInt($("#curindex").val()) + 1;

			if(nextIndex >= $('.formigo-slider-bottom-nav a').length) {		
				$('.formigo-slider').css({marginLeft:sliderWidth});	
				 nextIndex = 0;
			}

			clearInterval(delayId);
			formigoSlider.animateSlider(nextIndex, nextSlideWidth);

			if(autoplay == 1) {	
				formigoSlider.initTimer();	
			}

			return false;

		});	

		// Previous slide
		$('.formigo-slider-side-nav-previous a').click(function(event) {

			event.preventDefault();
			var nextSlideWidth = $(".formigo-slider-wrapper").width();
			var prevIndex = $("#curindex").val() - 1;

			if(prevIndex < 0 ) {
				$('.formigo-slider').css({marginLeft:sliderWidth});	
				 prevIndex = $('.formigo-slider-bottom-nav a').length - 1;
			}

			clearInterval(delayId);
			formigoSlider.animateSlider(prevIndex, nextSlideWidth);

			if(autoplay == 1) {
				formigoSlider.initTimer();
			}	

			return false;

		});

		// Bottom Navigation
		$('.formigo-slider-bottom-nav a').click(function(event) {

			event.preventDefault();
			var nextSlideWidth = $(".formigo-slider-wrapper").width();
			clearInterval(delayId);
			formigoSlider.animateSlider($(this).index(), nextSlideWidth);

			if(autoplay == 1) {
				formigoSlider.initTimer();
			}

			return false;

		});


		this.animateSlider = function(index, nextSlideWidth) {

			// The slider transition
			var newPos = (0 - (index + 1) * nextSlideWidth) + nextSlideWidth;
			$('.formigo-slider').animate({marginLeft: newPos}, {queue: false, duration: transitionSpeed});
			$('.formigo-slider-bottom-nav a').removeClass('formigo-slider-select');
			$('.formigo-slider-bottom-nav a').eq(index).addClass('formigo-slider-select');
			$("#curindex").val(index);	

		}

		
		this.moveSlider = function(index, nextSlideWidth) {
	
			// Used to reset following viewport change
			var newPos = (0 - (index + 1) * nextSlideWidth) + nextSlideWidth;
			$('.formigo-slider').css({marginLeft: newPos});
			$('.formigo-slider-bottom-nav a').removeClass('formigo-slider-select');
			$('.formigo-slider-bottom-nav a').eq(index).addClass('formigo-slider-select');
			$("#curindex").val(index);	

		}

		this.initTimer = function() {
		
			// Setup slider on timer
			clearInterval(delayId);
			delayId = setInterval(function() {
				var nextSlideWidth = formigoSlider.testSliderWidth();
				var nextIndex = $('.formigo-slider-bottom-nav a.formigo-slider-select').index() + 1;
				if(nextIndex >= $('.formigo-slider-bottom-nav a').length) {
					$('.formigo-slider').css({marginLeft:nextSlideWidth});	
					 nextIndex = 0;
				}
				formigoSlider.animateSlider(nextIndex, nextSlideWidth);
			}, delay);

		}	

		$(window).load(function() {
			
			// Initialise on window load (with images)
		    formigoSlider.testSliderWidth();

			// Autoplay?
			if(autoplay == 1) {
				formigoSlider.initTimer();
			}

			$(window).resize(function() {
				// This catches our viewport change, even though slider will load for correct viewport 
				var curSlideWidth,curIndex;
			  	curSlideWidth = formigoSlider.testSliderWidth();
				var curIndex = $('.formigo-slider-bottom-nav a.formigo-slider-select').index();
				formigoSlider.moveSlider(curIndex, curSlideWidth);
			});

			// Side navigation
			$('.formigo-slider').bind('mouseenter mouseleave', function(event) {
				switch(event.type) {
				    case 'mouseenter':
				       // when user enters the div
				       $(".formigo-slider-side-nav-next a").fadeIn("slow");
				       $(".formigo-slider-side-nav-previous a").fadeIn("slow");
				    break;
				    case 'mouseleave':
				      // leaves
				      $(".formigo-slider-side-nav-next a").fadeOut("slow");
				      $(".formigo-slider-side-nav-previous a").fadeOut("slow");
				    break;
				}	
			});

		});

	});
	
});