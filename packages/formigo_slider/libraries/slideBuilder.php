<?php   
	defined('C5_EXECUTE') or die("Access Denied.");
	
	class slideBuilder {
	
		public function buildSlideHtml($slide, $slideCounter) {
		
				$nextSlide = $slideCounter;

				$ih = Loader::helper('concrete/interface');
				$al = Loader::helper('concrete/asset_library');
				$form = Loader::helper('form');
				$fh = Loader::helper('form/color');
				Loader::element('editor_init');

				$slideHtml = '';

				if($slide['slideText'] !='') {

					$slideSummary = ' : '. substr(strip_tags($slide['slideText']),0,50) . '...';

				} else {

					$slideSummary = '';

				}		

				## Here we handball all this HTML together. Ho Hum		
				$slideHtml .= '<div id="slide-'.$nextSlide.'" class="slide" style="border-bottom:2px solid #cecece;">';
				$slideHtml .= '	<div id="slidetitlebar-'.$nextSlide.'" class="slidetitlebar" style="margin-bottom:10px;">';
				$slideHtml .= '		<div style="float:left;margin-right:100px;">';
				$slideHtml .= '			<!-- Title, Active, Delete, Order -->';
				$slideHtml .= '			<div style="font-weight:bold;margin-top:7px;font-size:15px;">'.t('Slide').' '.$nextSlide.'<span id="slidesummary-'.$nextSlide.'">'.$slideSummary.'</span></div>';
				$slideHtml .= '		</div>';
				$slideHtml .= '		<div style="float:right;">';

				$slideHtml .= '			'.$ih->button(t('Delete'),'javascript:void(0)','right','btn danger', array('onClick'=>'toggleDeleteSlide('.$nextSlide.');','style'=>'margin-top:-1px;width:60px;text-align:center;','id'=>'deleteslidebutton-'.$nextSlide));
				$slideHtml .= '			'.$ih->button(t('Edit'),'javascript:void(0)','right','btn primary editslidebutton', array('onClick'=>'toggleEditSlide('.$nextSlide.');','style'=>'margin-right:10px;margin-top:-1px;width:60px;text-align:center;','id'=>'editslidebutton-'.$nextSlide));
				$slideHtml .= '		</div>';			
				$slideHtml .= '		<div style="float:right;">';

				$slideHtml .= '			'.t('Active').'&nbsp;'.$form->checkbox('slideActive-'.$nextSlide, 1, $slide['slideActive']); 
				$slideHtml .= '			&nbsp;&nbsp;';
				$slideHtml .= '			'.t('Order').'&nbsp;'.$form->text('slideOrder-'.$nextSlide, $nextSlide, array('style'=>'width:30px')); 
				$slideHtml .= '			&nbsp;&nbsp;';
				$slideHtml .= '		</div>';

				$slideHtml .= '		<div style="clear:both;"></div>';		
				$slideHtml .= '	</div>';
				$slideHtml .= '	<div id="slideoptions-'.$nextSlide.'" class="slideoptions" style="display:none;">';

				$slideHtml .= '		<div class="slide-left">';

				$slideHtml .= '			<div class="clearfix">';
				$slideHtml .= '				<label style="text-align:left;width:150px;">';	
				$slideHtml .= '				'.t('Template');		
				$slideHtml .= '				</label>';
				$slideHtml .= '				<div class="input">';
				$slideHtml .= '					'.$form->select('slideTemplate-'.$nextSlide, array(''=>t('Please Select'),'imageleft'=>t('Image Left'),'imageright'=>t('Image Right'),'fullimage'=>t('Full Width Image'), 'fulltext'=>t('Full Width Text')), $slide['slideTemplate'], array('style'=>'width:150px;'));
				$slideHtml .= '					<span class="help-block">'.t('Choose a template for this slide').'</span>';
				$slideHtml .= '				</div>';
				$slideHtml .= '			</div>';

				$slideHtml .= '			<div class="clearfix">';
				$slideHtml .= '				<label style="text-align:left;">';	
				$slideHtml .= '				'.t('Image');		
				$slideHtml .= '				</label>';
				$slideHtml .= '				<div class="input">';
				$slideHtml .= '					'.$al->image('slideImage-'.$nextSlide, 'slideImage-'.$nextSlide, t('Choose File'), File::getByID($slide['slideImage']));
				$slideHtml .= '					<span class="help-block">'.t('Choose an image for the slide').'</span>';
				$slideHtml .= '				</div>';
				$slideHtml .= '			</div>';	

				$slideHtml .= '			<div class="clearfix">';
				$slideHtml .= '				<label style="text-align:left;">';	
				$slideHtml .= '				'.t('Image ALT text');		
				$slideHtml .= '				</label>';
				$slideHtml .= '				<div class="input">';
				$slideHtml .= '					'.$form->text('slideImageAltText-'.$nextSlide, $slide['slideImageAltText'], array('style'=>'width:150px;'));
				$slideHtml .= '					<span class="help-block">'.t('Set the Alt text for the image').'</span>';
				$slideHtml .= '				</div>';
				$slideHtml .= '			</div>';		

				$slideHtml .= '		</div>';

				$slideHtml .= '		<div class="slide-right">';	
				$slideHtml .= '				'.$form->textarea('slideText-'.$nextSlide, $slide['slideText'], array('class' =>'ccm-advanced-editor'));
				$slideHtml .= '				<span class="help-block">'.t('Enter the text/html content for the slide').'</span>';
				$slideHtml .= '		</div>';
				$slideHtml .= '		<div style="clear:both;"></div>';			
				$slideHtml .= '	</div>';	
				$slideHtml .= '</div>';

				return $slideHtml;		
			
		}	
		
	}	