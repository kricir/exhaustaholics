<?php   
defined('C5_EXECUTE') or die(_("Access Denied."));

class FormigoSliderPackage extends Package {

    protected $pkgHandle = 'formigo_slider';
    protected $appVersionRequired = '5.5.0';
    protected $pkgVersion = '1.0.2';

    public function getPackageDescription() {
	 
        return t("A well featured Content Slider block to enhance any Concrete5 website");
     
	}

    public function getPackageName() {
     
		return t("Formigo Slider");
     
	}
	
    public function install() {
	
		$pkg = parent::install();	
		BlockType::installBlockTypeFromPackage('formigo_slider', $pkg); 
	     	 
    }
	 
	public function uninstall() {

        $pkg = parent::uninstall();
		
		## Remove btFormigoSliderSlides table
		$db = Loader::db();
		$db->execute('drop table btFormigoSliderSlides;');
		
    }
     
}
?>