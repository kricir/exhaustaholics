<?php 
defined('C5_EXECUTE') or die(_("Access Denied."));

/**
 * Config Info by John Liddiard (aka JohntheFish)
 * www.jlunderwater.co.uk, www.c5magic.co.uk
 * This software is licensed under the terms described in the concrete5.org marketplace.
 * Please find the add-on there for the latest license copy.
 */

class DashboardSystemEnvironmentConfigInfoController extends Controller {

	public function on_start(){
		$this->addHeaderItem($this->some_css());

		$html = Loader::helper('html');
		$this->traditional();
	}

	public function traditional(){
		if(version_compare(Config::get('SITE_APP_VERSION'), 5.5, 'lt')) {
			$this->set('traditional', true);
			$this->set('fivefive', false);
			return true;
		}
		$this->set('traditional', false);
		$this->set('fivefive', true);
		return false;
	}

	public function fivefive(){
		return ! $this->traditional ();
	}

	public function view() {

		$poss_info_files = $this->list_poss_info_files();
		$this->set('poss_info_files',$poss_info_files);

		// See what was selected. Should only be one file
		$info_files = array();
		foreach ($_POST as $ky=>$val){
			if (preg_match("/ccm\-submit\-/i",$ky)){
				if (array_key_exists ($val, $poss_info_files)){
					$info_files[] = $val;
				}
			}
		}
		if (empty($info_files)){
			$info_files[] = 'site.php';
		}

		$selected_filename .= implode(' ',$info_files);
		$this->set('selected_filename',$selected_filename);

		$plain_content = $this->get_config_file_content($selected_filename);

		$this->set('file_contents', $plain_content);
		$this->set('syn_file_contents', $this->syntax_format($plain_content, $selected_filename));
	}

	/*
	List all files in config. 
	The output is not the cleanest way to do it, but makes everything else an easy 
	hack from existing code.
	*/
	private function list_poss_info_files(){
		$file = Loader::helper('file');
		$l = $file->getDirectoryContents(DIR_CONFIG_SITE);
		if (empty($l)){
			$l = array ('site.php');
		}
		$l2 = array();
		foreach ($l as $filename){
			$l2[$filename] = DIR_CONFIG_SITE.'/'.$filename;
		}

		// tack the site .htaccess on the end
		if (file_exists(DIR_BASE.'/.htaccess')){
			$l2['.htaccess']  = DIR_BASE.'/.htaccess';
		}
		return $l2;
	}

	/*
	Load the file content or report an error.
	A bit over-defensive, but better safe than sorry
	*/
	private function get_config_file_content($selected_filename){
	
		$poss_info_files = $this->list_poss_info_files();
		$filepath = $poss_info_files[$selected_filename];
		
		if (!file_exists($filepath)){
			$m = t('File %s does not exist', $selected_filename);
			$error = Loader::helper('validation/error');
			$error->add($m);
			$this->set('error', $error);
			return;
		}

		try {
        	$file = Loader::helper('file');
			return $file->getContents($filepath) ;
		}
		
		catch (Exception $e) {
			$m = t("Exception when reading file %s:\n%s", $selected_filename, $e->getMessage());
			$error = Loader::helper('validation/error');
			$error->add($m);
			$this->set('error', $error);
			return;
		}
	}

	/* 
	Format as nicely as we can
	http://qbnz.com/highlighter/geshi-doc.html
	*/
	private function syntax_format($code,$filename) {
	
		if (empty($filename)){
			$language = 'php';
		} else {
			$file = Loader::helper('file');
			$language = $file->getExtension($filename);
			if ($language == 'htaccess'){
				$language = 'apache';
			}
		}
		if (empty($language)){
			$language = 'php';
		}
		
		try {
			// This decode will also hold through to the handler
			$code = rawurldecode ($code);

			$p = Package::getByHandle('syntax_highlighter');
			if(!$p){
				// The exception could show up in the error log.
				throw new Exception(t('Highlighted Code Block not installed. It can be installed from http://www.concrete5.org/marketplace/addons/highlighted_code_block/'));
			}

			Loader::library("geshi", "syntax_highlighter");
			$geshi = new GeSHi($code, $language);
			$geshi->enable_line_numbers(GESHI_NORMAL_LINE_NUMBERS);
			$comment_colour = 'green';
			$geshi->set_comments_style('MULTI', 'color: '.$comment_colour.';', true);
			$geshi->set_comments_style(1, 'color: '.$comment_colour.';', true);
			$geshi->enable_strict_mode(true);
			$geshi->enable_keyword_links(false);
			
			$syn_code = $geshi->parse_code();
			
			// check for syntax highlight error
			$err = $geshi->error();
			if ($err){
				$error = Loader::helper('validation/error');
				$error->add($m);
				$this->set('error', $err);
			}

			return 	'<div class="jl_listing">'.
					$syn_code.
					'</div>';

		}
		// else, just output in a div/pre.
		catch (Exception $e) {
			return 	'<div class="jl_listing" >'.
					'<pre>'.htmlentities($code).'</pre>'.
					'</div>'.
					'<small>'.
					t('Install the <a href="http://www.concrete5.org/marketplace/addons/highlighted_code_block/">Highlighted Code Block</a> for prettier output.').
					'</small>';
		}
	}


	/**
	 * Some css to manage the output format
	 */
	public function some_css (){
		$url = Loader::helper("concrete/urls");

		ob_start();
		?>
		<style type="text/css">
		#selectable_text{
			width:100%;
			position: relative;
			display: block;
		}
		#selectall{
			top: 7px;
			right: 10px;
			position: absolute;
			display: block;
		}
		#copytmp,#copytmp:focus{
			position: absolute;
			top: 0;
			left: 0;
			border: none;
			outline: none;
			resize: none;
			overflow: hidden;
			-webkit-box-shadow: none;
      		-moz-box-shadow: none;
            box-shadow: none
            }
        #config_info_form div.jl_listing{
 			position: relative;
       		list-style: decimal outside none;
        }
        #config_info_form div.jl_listing pre{
       		padding: 8.5px;
       		border: 1px solid #CCC;
       		line-height: 18px;
			-webkit-border-radius: 3px;
			-moz-border-radius: 3px;
			border-radius: 3px;
        }
        div.jl-top-footer {
			-webkit-border-radius: 0 0 0 0;
			-moz-border-radius: 0 0 0 0;
			border-radius: 0 0 0 0;
		}
		</style>
		<?php 
		$css = ob_get_contents();
		ob_end_clean();
		return $css;
	}

}