<?php 
defined('C5_EXECUTE') or die(_("Access Denied."));

/**
 * Config Info by John Liddiard (aka JohntheFish)
 * www.jlunderwater.co.uk, www.c5magic.co.uk
 * This software is licensed under the terms described in the concrete5.org marketplace.
 * Please find the add-on there for the latest license copy.
 */

if ($fivefive){
	$h = Loader::helper('concrete/dashboard');
	echo $h->getDashboardPaneHeaderWrapper(	t('Configuration file: %s',$selected_filename),
											t('Shows Concrete5 configuration info for your site/host environment. See <a href="http://www.concrete5.org/documentation/installation/installing_concrete5/" target="_blank">Concrete5 Basic Setup</a> for more details about the Concrete5 site configuration files. Take care not to disclose password or other security information if copying to an email or problem report.'),
											'',
											'',
											array(),
											false);
	// text for dialogs - only used in 5.5+
	$txt_select_all = t('Select All');
} else {
	?>
	<h1><span><?php  echo t('Config Info %s',$selected_filename);?></span></h1>
	<div class="ccm-dashboard-inner">
	<?php 
}

?>
<form method="post" id="config_info_form" action="<?php  echo $this->action('view')?>">
	<div class="ccm-pane-footer jl-top-footer">
		<?php 
			$ih = Loader::helper('concrete/interface');
			foreach (array_reverse($poss_info_files) as $ky=>$val){
				echo '&nbsp;';
				echo $ih->submit($ky, $ky,'right','primary');
			}
		?>
	</div>
	<div class="ccm-pane-body">
		<div>
			<?php 
				if ($file_contents !== null){
					?>
					<div id="raw_text" style="display:none">
						<pre><?php  echo htmlentities($file_contents);?></pre>
					</div>
					<div id="selectable_text">
						<?php  echo $syn_file_contents;?>
					</div>
					<?php 
				}
			?>
		</div>
	</div>

	<div style="clear:both;"></div>
	<div class="ccm-pane-footer">
		<?php 
			$ih = Loader::helper('concrete/interface');
			echo $ih->button_js(t('Cancel'), 'ccm_closeDashboardPane(this)', 'left');

			foreach (array_reverse($poss_info_files) as $ky=>$val){
				echo '&nbsp;';
				echo $ih->submit($ky, $ky,'right','primary');
			}
		?>
	</div>
</form>
<?php 
if ($traditional){
	?>
	</div>
<?php 
}
?>
<script type="text/javascript">
$(document).ready(function(){var a="<?php  echo $txt_select_all;?>";if($.browser.msie){a=""}if(!a){return}$("#selectable_text div.jl_listing").after('<a id="selectall" href="#selectall">'+a+"</a>");$("#selectall").click(function(){if($("#copytmp").length){$("#copytmp").remove()}var a=$("#raw_text pre").text();var b=$("#selectable_text pre");var c=$(b).css("font-family");var d=$(b).css("font-size");var e=$(b).css("line-height");var f=$(b).css("margin");var g=$(b).css("padding");var h=$(b).css("background");var i=$(b).css("border");var j=$(b).css("outline");var k=$(b).width();var l=$(b).height();$('<textarea id="copytmp">').appendTo($("#selectable_text")).val(a).focus().select().width(k).height(l).css("background",h).css("border",i).css("outline",j).css("padding",g).css("margin",f).css("line-height",e).css("font-family",c).css("font-size",d).position({my:"bottom right",at:"bottom right",of:$(b)});$("#copytmp").one("click blur",function(){$("#copytmp").remove();return false});var m;$("#copytmp").on("keyup",function(){m=false});$("#copytmp").on("keydown",function(a){var b=a.keyCode;var c=17;var d=67;if(b==c){m=true;return true}if(b==d&&m){return true}m=false;return false});return false})})
</script>


