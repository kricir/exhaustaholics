<?php 
defined('C5_EXECUTE') or die(_("Access Denied."));

/**
 * Config Info by John Liddiard (aka JohntheFish)
 * www.jlunderwater.co.uk, www.c5magic.co.uk
 * This software is licensed under the terms described in the concrete5.org marketplace.
 * Please find the add-on there for the latest license copy.
 */

class JlConfigInfoPackage extends Package {

	protected $pkgHandle = 'jl_config_info';
	protected $appVersionRequired = '5.4';
	protected $pkgVersion = '1.0';

	public function getPackageDescription() {
		return t('View the Concrete5 site configuration files.');
	}

	public function getPackageName() {
		return t('Config Info');
	}

	public function install() {
		$pkg = parent::install();

		Loader::model('single_page');
		$spid = SinglePage::add('/dashboard/system/environment/config_info', $pkg);
		$spid->update(array('cName'=>$this->getPackageName(), 'cDescription' => $this->getPackageDescription() ));
	}

}